def eliminate_left_recursion(grammar):
    new_grammar = {}

    for non_terminal in grammar:
        productions = grammar[non_terminal]
        alpha, beta = split_productions(productions, non_terminal)

        if alpha:
            new_non_terminal = non_terminal + "_"
            new_grammar[non_terminal] = []

            for prod in beta:
                new_grammar[non_terminal].append(prod + new_non_terminal)

            for prod in alpha:
                new_grammar[new_non_terminal] = [prod + new_non_terminal]

            new_grammar[new_non_terminal].append('ε')

        else:
            new_grammar[non_terminal] = productions

    return new_grammar
def split_productions(productions, non_terminal):
    alpha = []
    beta = []

    for prod in productions:
        if prod and prod[0] == non_terminal:
            alpha.append(prod[1:])
        else:
            beta.append(prod)

    return alpha, beta

def factor_common_prefix(grammar):
    new_grammar = {}

    for non_terminal in grammar:
        productions = grammar[non_terminal]
        common_prefixes = find_common_prefixes(productions)

        if common_prefixes:
            new_non_terminal = non_terminal + "p"

            for prefix in common_prefixes:
                new_grammar[new_non_terminal] = [prefix + non_terminal]

            for prod in productions:
                for prefix in common_prefixes:
                    if prod.startswith(prefix):
                        new_grammar[new_non_terminal].append(prod[len(prefix):])

        else:
            new_grammar[non_terminal] = productions

    return new_grammar


def find_common_prefixes(productions):
    prefixes = set()

    for i in range(len(productions[0]) + 1):
        common_prefix = productions[0][:i]

        if all(prod.startswith(common_prefix) for prod in productions):
            prefixes.add(common_prefix)

    return list(prefixes)


# Example Usage:
grammar = {
    'A': ['B','C','a'],
    'B': ['bBCa','f'],
    'C': ['bAB', '']
}

eliminated_left_recursion_grammar = eliminate_left_recursion(grammar)
factored_grammar = factor_common_prefix(eliminated_left_recursion_grammar)

print("Original Grammar:")
print(grammar)
print("\nGrammar after eliminating left recursion:")
print(eliminated_left_recursion_grammar)
print("\nGrammar after factoring common prefixes:")
print(factored_grammar)
