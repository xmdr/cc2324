#ifndef EXPR_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#define EXPR_H

#ifdef DEBUG
#define debug printf
#define dbgExpr(e) dumpExpr(e,0)
#else
#define debug(...)
#define dbgExpr(...)
#endif

typedef struct Expr *Expr;
struct Expr {
	union {
		struct {
			Expr lhs;
			Expr rhs;
		};
		struct {
			double val;
			int precision; // how many decimals after the . (0 = int)
		};
		char id;
	};
	int line;
	int print;
	enum {ADD, SUB, MUL, DIV, POW, VAL, IDN, SEQ, EQL, OPCOUNT} op;
};

const char* op2str(int op){
	return (const char*const[OPCOUNT]){
		[MUL]="MUL",[DIV]="DIV",[ADD]="ADD",[SUB]="SUB",
		[POW]="POW",[VAL]="VAL",[IDN]="IDN",[SEQ]="SEQ",
		[EQL]="EQL",
	}[op];
}

Expr makeBinaryExpr(int line, int operator, Expr lhs, Expr rhs){
	Expr e = calloc(1, sizeof *e);
	e->op = operator;
	e->lhs = lhs;
	e->rhs = rhs;
	e->line = line;
	debug("Line %d New @ %p: %s %p %p \n", line, e, op2str(operator), lhs, rhs);
	return e;
}

Expr makeValueExpr(int line, double value, int prec){
	Expr e = makeBinaryExpr(line, VAL, NULL, NULL);
	e->val = value;
	e->precision = prec;
	return e;
}

Expr makePrecisionNumber(int line, int n, char text[n]){
	char *dot = strchr(text, '.');
	int prec = dot != NULL
		? n-(dot-text)-1
		: 0;
	return makeValueExpr(line, atof(text), prec);
}

Expr makeNegExpr(int line, Expr expr){
	return makeBinaryExpr(line, MUL, expr, makeValueExpr(line, -1, 0));
}

Expr makeIdent(int line, char x){
	assert(x <= 'z' && x >= 'a');
	Expr e = makeBinaryExpr(line, IDN, NULL, NULL);
	e->id = x;
	return e;
}

typedef struct {double val; int isset;} Context[26];
double evalCtx(Expr e, Context c){
	double ret;
	switch(e->op){
		case VAL: ret = e->val; break;
		case IDN:
			if (c[e->id-'a'].isset){
				ret = c[e->id-'a'].val;
			} else {
				printf("Error in line %d: variable '%c' has no initialized value.\n", e->line, e->id);
				exit(0);
			}
			break;
		case ADD: ret = evalCtx(e->lhs,c) + evalCtx(e->rhs,c); break;
		case SUB: ret = evalCtx(e->lhs,c) - evalCtx(e->rhs,c); break;
		case MUL: ret = evalCtx(e->lhs,c) * evalCtx(e->rhs,c); break;
		case DIV: ret = evalCtx(e->lhs,c) / evalCtx(e->rhs,c); break;
		case SEQ: ret = evalCtx(e->lhs,c) , evalCtx(e->rhs,c); break;
		case POW: ret = pow(evalCtx(e->lhs,c), evalCtx(e->rhs,c)); break;
		case EQL: {
			assert(e->lhs && e->lhs->op == IDN);
			int i = e->lhs->id-'a';
			c[i].isset=1;
			c[i].val = evalCtx(e->rhs,c);
			ret = c[i].val;
			break;
		}
		case OPCOUNT: exit(1);
	}
	if (e->print){
		printf("%lf\n", ret);
	}
	return ret;
}

double evalExpr(Expr e){
	Context vars = {0};
	return evalCtx(e, vars);
}

void dumpExpr(Expr e, int d){
	printf("%*s%p: ",d,"",e);
	if (e == NULL){
		return;
	}
	if (e->op == IDN){
		printf("%c\n", e->id);
		return;
	}
	if (e->op == VAL){
		printf("%f (%d)\n", e->val, e->precision);
		return;
	}
	puts(op2str(e->op));
	dumpExpr(e->lhs, d+4);
	dumpExpr(e->rhs, d+4);
}

void freeExpr(Expr e){
	if (e == NULL) return;
	if (e->op != IDN && e->op != VAL){
		freeExpr(e->lhs);
		e->lhs = NULL;
		freeExpr(e->rhs);
		e->rhs = NULL;
	}
	free(e);
}
#endif
