{ 
/**********************************************/
#include <stdio.h>
#include <stdlib.h>
#include "expr.c"

extern char *yytext;
extern int yyleng;
extern int line;
/**********************************************/
}

%start LLparser, Input;
%token NUMBER, PLUS, MINUS, TIMES, DIVIDE, CARET, IDENT, EQUALS, SEMI, LET, LEFT_PARENTHESIS, RIGHT_PARENTHESIS;
%options "generate-lexer-wrapper";
%lexical yylex;

Input
{Expr e;}
  : Program(&e)
    { 
      dbgExpr(e);
      evalExpr(e);
      freeExpr(e);
    }
  ;

Program(Expr *e)
{Expr rhs;}
  : Statement(e) [Statement(&rhs) {*e = makeBinaryExpr(line, SEQ, *e, rhs);}]*
  ;

Statement(Expr *e)
  : Expression(e) {(**e).print=1;} SEMI
  | Assign(e) SEMI
  ;

Assign(Expr *e)
{Expr id, what;}
  : LET Ident(&id) EQUALS Expression(&what) {*e = makeBinaryExpr(line, EQL, id, what);}
  ;

Expression(Expr *e)
{int operator; Expr rhs;}
  : Term(e)
    [[PLUS {operator=ADD;} | MINUS {operator=SUB;}]
      Term(&rhs) {*e = makeBinaryExpr(line, operator, *e, rhs);}
    ]*
  ;

Term(Expr *t)
{int operator; Expr rhs;}
  : Factor(t)
    [[TIMES {operator=MUL;} | DIVIDE {operator=DIV;}]
      Factor(&rhs) {*t = makeBinaryExpr(line, operator, *t, rhs);}
    ]*
  ;

Factor(Expr *f)
{Expr rhs;}
  : Primary(f)
    [[CARET]
      Factor(&rhs) {*f = makeBinaryExpr(line, POW, *f, rhs);}
    ]?
  ;

Ident(Expr *e)
  : IDENT {*e = makeIdent(line, *yytext);}
  ;

Primary(Expr *f)
  : NUMBER { *f = makePrecisionNumber(line, yyleng, yytext); }
  | Ident(f)
  | MINUS Primary(f) { *f = makeNegExpr(line, *f); }
  | LEFT_PARENTHESIS Expression(f) RIGHT_PARENTHESIS
  ;

{ 
/*****************************************************************/
/* the following code is copied verbatim in the generated C file */

void LLmessage(int token) {
  printf("Syntax error....abort\n");
  exit(0);
}

int main() {
  LLparser();
  return 0;
}

/*****************************************************************/
}
