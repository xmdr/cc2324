#ifndef EXPR_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define EXPR_H

typedef struct Expr *Expr;
struct Expr {
	enum {ADD, SUB, MUL, DIV, /*NEG,*/ VAL, OPCOUNT} op;
	Expr lhs;
	Expr rhs;
	double val; // what is a union
	int precision; // how many decimals after the . (0 = int)
};

Expr makeBinaryExpr(int operator, Expr lhs, Expr rhs){
	Expr e = calloc(1,sizeof(*e));
	e->op = operator;
	e->lhs = lhs;
	e->rhs = rhs;
	return e;
}

Expr makeValueExpr(double value, int prec){
	Expr e = makeBinaryExpr(VAL, NULL, NULL);
	e->val = value;
	e->precision = prec;
	return e;
}

Expr makePrecisionNumber(int n, char text[n]){
	char *dot = strchr(text, '.');
	int prec = dot != NULL
		? n-(dot-text)-1
		: 0;
	return makeValueExpr(atof(text), prec);
}

Expr makeNegExpr(Expr expr){
	// the assignment doesn't say it, but
	// this appears to be mul by -1 for some reason
	return makeBinaryExpr(MUL, expr, makeValueExpr(-1, 0));
	//return makeBinaryExpr(NEG, expr, NULL);
}

double evalExpr(Expr e){
	switch(e->op){
		case VAL: return e->val;
		// case NEG: return -evalExpr(e->lhs);
		case ADD: return evalExpr(e->lhs) + evalExpr(e->rhs);
		case SUB: return evalExpr(e->lhs) - evalExpr(e->rhs);
		case MUL: return evalExpr(e->lhs) * evalExpr(e->rhs);
		case DIV: return evalExpr(e->lhs) / evalExpr(e->rhs);
		case OPCOUNT: return (double)0["what did you do"];
	}
}

const char* op2str(int op){
	return (const char*const[OPCOUNT]){
		[MUL]="MUL",[DIV]="DIV",[ADD]="ADD",[SUB]="SUB",
		[VAL]="VAL"
	}[op];
}

void dbgExpr(Expr e){
	void helper(Expr e, int d){
		printf("%*s",d,"");
		if (e == NULL){
			puts("NULL");
			return;
		}
		if (e->op == VAL){
			printf("%f (%d)\n", e->val, e->precision);
			return;
		}
		puts(op2str(e->op));
		d+=4;
		//printf("%*sLHS %p\n",d,"", e->lhs);
		helper(e->lhs, d);
		//printf("%*sRHS %p\n",d,"", e->rhs);
		helper(e->rhs, d);
	}
	helper(e, 0);
}

void stackExpr(Expr e){
	if (e->op == VAL){
		printf("PUSH %.*f\n", e->precision, e->val);
		return;
	}
	stackExpr(e->lhs);
	stackExpr(e->rhs);
	puts(op2str(e->op));
}

void freeExpr(Expr e){
	if (e == NULL) return;
	freeExpr(e->lhs);
	e->lhs = NULL;
	freeExpr(e->rhs);
	e->rhs = NULL;
}
#endif
