{ 
/**********************************************/
#include <stdio.h>
#include <stdlib.h>
#include "expr.c"

extern char *yytext;
extern int yyleng;
/**********************************************/
}

%start LLparser, Input;
%token NUMBER, PLUS, MINUS, TIMES, DIVIDE, 
       LEFT_PARENTHESIS, RIGHT_PARENTHESIS, NEWLINE;
%options "generate-lexer-wrapper";
%lexical yylex;

Input
{Expr e;}
  : [[Expression(&e)
     { //dbgExpr(e);
       stackExpr(e);
       //printf("%lf\n", evalExpr(e));
       freeExpr(e);
     }
     ]? NEWLINE ]*
  ;

Expression(Expr *e)
{int operator; Expr rhs;}
  : Term(e)
    [[PLUS {operator=ADD;} | MINUS {operator=SUB;}]
      Term(&rhs) {*e = makeBinaryExpr(operator, *e, rhs);}
    ]*
  ;

Term(Expr *t)
{int operator; Expr rhs;}
  : Factor(t)
    [[TIMES {operator=MUL;} | DIVIDE {operator=DIV;}]
      Factor(&rhs) {*t = makeBinaryExpr(operator, *t, rhs);}
    ]*
  ;

Factor(Expr *f)
  : NUMBER { *f = makePrecisionNumber(yyleng, yytext); }
  | MINUS Factor(f) { *f = makeNegExpr(*f); }
  | LEFT_PARENTHESIS Expression(f) RIGHT_PARENTHESIS
  ;

{ 
/*****************************************************************/
/* the following code is copied verbatim in the generated C file */

void LLmessage(int token) {
  printf("Syntax error....abort\n");
  exit(0);
}

int main() {
  LLparser();
  return 0;
}

/*****************************************************************/
}
