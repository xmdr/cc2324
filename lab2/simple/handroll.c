#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define true 1
#define false 0
#define bool int
#define gotta(x) if(!(x)) return false

#ifdef DEBUG
#define debug printf
#else
#define debug(...) 
#endif

bool eat(int s, int _){
	debug("%*s(expecting '%c', ",_,"",s);
	int in; do {
		in = getchar();
	} while (isspace(in));
	if (in == s){
		debug("ok)\n");
		return true;
	} else {
		ungetc(in, stdin);
		debug("fail)\n");
		return false;
	}
}

bool parse(char which, int _){
	debug("%*sTrying %c...\n", _++, "", which);
	switch (which) {
	case 'S':
		gotta(parse('A',_));
		gotta(parse('B',_));
		return true;
	case 'A':
		if (eat('a',_)){
			return true;
		} else if (eat('(',_)) {
			gotta(parse('S',_));
			gotta(eat(')',_));
			return true;
		}
		return false;
	case 'B':
		if (parse('S',_)) return true;
		if (eat('+',_)) return parse('S',_);
		if (eat('*',_)) return parse('B',_);
		return true;
	}
	exit(-1);
}

int main(){
	if (parse('S',0) && eat(EOF,0)){
		puts("SUCCESS");
	} else {
		puts("FAILURE");
	}
}