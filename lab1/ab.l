%{
#include <stdio.h>

int a_count = 0;
int b_count = 0;
char *ugly_hack; // Probably invokes undefined behavior somewhere
%}

%x IN_LINE

%%
<IN_LINE>a { a_count++; }
<IN_LINE>b { b_count++; }
<IN_LINE>\n {
	char *verdict = (a_count >= 2 && b_count >= 3)
	              ? "accepted"
                  : "rejected";
    printf("%s: %s", verdict, ugly_hack);
    a_count = 0;
    b_count = 0;
    BEGIN(INITIAL);
}
<IN_LINE>. {/* suppress echo */}
. {
	BEGIN(IN_LINE);
	a_count = *yytext=='a';
	b_count = *yytext=='b';
	ugly_hack = yytext; }
.|\n {/* suppress trailing newline for local bitch on the block known as themis */}

%%

int yywrap(){/* magic incantation to make the linker shut up*/};

int main() {
    yylex();
    return 0;
}
