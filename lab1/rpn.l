%{
#include <stdlib.h>
// I have really mixed feelings about this
double stack[999] = {0};
int top = 0;

#define binop(oper) {\
	double r = stack[--top];\
	double l = stack[--top];\
	stack[top++] = l oper r;\
}

%}

%%
\n {printf("%f\n", stack[--top]);}
-?[0-9.]+([Ee][+-][0-9]+)? {stack[top++] = strtod(yytext, &(char*){"I don't care about error handling"});}
"-" binop(-)
"+" binop(+)
"*" binop(*)
"/" binop(/)
. {}
%%

int yywrap(){/* shut up shut up SHUT UP */};
int main() {
    yylex();
    return 0;
}
