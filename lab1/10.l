%{
%}

%%

^(0|10)+$ {printf("accepted: %s\n", yytext);}
^.+$ {printf("rejected: %s\n", yytext);}
\n {}
%%

int yywrap(){}
int main(){
	yylex();
	return 0;
}