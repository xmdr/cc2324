%{

void arabify(const char *yytext){
	// assuming up to 1 lesser numeral before a higher numeral appears
	int lut[255] = {
		['M']=1000,
		['D']=500,
		['C']=100,
		['L']=50,
		['X']=10,
		['V']=5,
		['I']=1,
		// default is 0
	};
	const char *ptr = yytext;
	int sum = 0;
	int prev = 0;
	do {
		int val = lut[*ptr];
		if (val <= prev){
			// "VI" == 5+1
			sum += val;
		} else {
			// "IV" == 5-1
			sum -= prev*2;
			sum += val;
		}
		prev = val;
	} while(*(ptr++));
	printf("%s=%d", yytext, sum);
}

%}

%%
[MDCLXVI]+ {arabify(yytext);}
.          {putchar('\n');} // where does the trailing nl come from??
%%

int yywrap(){}
int main(){
	yylex();
	return 0;
}
