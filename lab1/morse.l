%{
%}

%%
[Aa]	{printf(".-");}
[Bb]	{printf("-...");}
[Cc]	{printf("-.-.");}
[Dd]	{printf("-..");}
[Ee]	{printf(".");}
[Ff]	{printf("..-.");}
[Gg]	{printf("--.");}
[Hh]	{printf("....");}
[Ii]	{printf("..");}
[Jj]	{printf(".---");}
[Kk]	{printf("-.-");}
[Ll]	{printf(".-..");}
[Mm]	{printf("--");}
[Nn]	{printf("-.");}
[Oo]	{printf("---");}
[Pp]	{printf(".--.");}
[Qq]	{printf("--.-");}
[Rr]	{printf(".-.");}
[Ss]	{printf("...");}
[Tt]	{printf("-");}
[Uu]	{printf("..-");}
[Vv]	{printf("...-");}
[Ww]	{printf(".--");}
[Xx]	{printf("-..-");}
[Yy]	{printf("-.--");}
[Zz]	{printf("--..");}
0	{printf("-----");}
1	{printf(".----");}
2	{printf("..---");}
3	{printf("...--");}
4	{printf("....-");}
5	{printf(".....");}
6	{printf("-....");}
7	{printf("--...");}
8	{printf("---..");}
9	{printf("----.");}
,	{printf("--..--");}
:	{printf("---...");}
\?	{printf("..--..");}
\/	{printf("-..-.");}
\\	{printf(".-..-.");}
=	{printf("-...-");}
.	{printf("%s", yytext);}
%%

int yywrap(){}
int main(){
	yylex();
	return 0;
}