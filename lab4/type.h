#pragma once

#include "diag.h"
#include "prelude.h"

typedef enum {
	kundecided=0, // default
	kvoid,        // pseudo type for unifying procedures and functions
	kbool,        // peusdo type for guard analysis
	karr, kfn,  // composite types
	kint, kreal,  // basic types
	kinvalid      // indicates that the item failed semantic analysis
} Kind;

typedef struct {
	i32 lwb, upb;
} Range;

typedef struct Type Type;
struct Type {
	union {
		struct /* array */ {
			i32 lwb, upb;
			Kind elem;
		};
		struct /* function */ {
			Type *argTypes; // Type argTypes[nArgs];
			u32 nArgs;
			Kind retn;
		};
	};

	Kind kind:30;
	u32 isConst:1; // is not mutable
	u32 isRef:1;   // "var" args in procs
};

Type newType_arr(Kind elem, i32 lwb, i32 upb);
Type newType_int(u32 isRef, u32 isConst);
Type newType_real(u32 isRef, u32 isConst);
Type newType_undecided(void);

void fmtType(Type *, char **dst, size_t *n);

/**
 * \brief Checks if a type fits the constraints
 * \param g The given type
 * \param e The type that is expected
 * \out warn If not null, is a heap allocated string indicating a type warning
 * \out err If not null, is a heap allocated string indicating a type error
 */
void typeCheck(Type g, Type e, char **warn, char **err);
