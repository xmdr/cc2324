#!/bin/bash

# Compile C program
mkdir -p out
gcc strings_paduse.c -o out/strings_normal || exit 1
out/strings_normal < dicts/cats > out/cats0.dot
dot -Tpng out/cats0.dot -o out/cats0.png

# Loop to create shuffled DOT files
for i in {1..3}; do
    out/strings_normal < <(shuf dicts/cats) > out/cats"$i".dot
    dot -Tpng out/cats"$i".dot -o out/cats"$i".png
done
