#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Function declaration
int $fn_return42(/*ctype*/ int $n){
int $ret;
/*ctype*/ int $2 = $n; // gen -> rule ident
int $3 = 42; // int lit
double $1 = (double)$2 > (double)$3; // binary op
if (!$1) goto L1; // goto else if not guard
// Call site ###
// begin reify VALUE arg 0
/*ctype*/ int $5 = $n; // gen -> rule ident
int $6 = 1; // int lit
double $4 = (double)$5 - (double)$6; // binary op
// end reify arg 0
/*ctype*/ int $7 = $fn_return42($4);
/*ctype*/ int *$8 = &$ret; // retval LHS
*$8 = $7; // assign L-value 
goto L2;
L1:;
/*ctype*/ int $9 = $n; // gen -> rule ident
/*ctype*/ int *$10 = &$ret; // retval LHS
*$10 = $9; // assign L-value 
// while loop (start=L3, end=L4)
L3:;
// while guard start
/*ctype*/ int $12 = $ret; // retval RHS
int $13 = 42; // int lit
double $11 = (double)$12 < (double)$13; // binary op
// while guard end
if (!$11) goto L4; // goto else if not guard
// while body start
/*ctype*/ int $15 = $ret; // retval RHS
int $16 = 1; // int lit
double $14 = (double)$15 + (double)$16; // binary op
/*ctype*/ int *$17 = &$ret; // retval LHS
*$17 = $14; // assign L-value 
// while body end
goto L3;
L4:;
L2:;
return $ret;
}



int main(void){
// Call site ###
// begin reify VALUE arg 0
int $18 = 3; // int lit
// end reify arg 0
/*ctype*/ int $19 = $fn_return42($18);
(void)printf("%d\n",$19);
}
