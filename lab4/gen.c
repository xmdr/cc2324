#include "prelude.h"

// Number of used labels and registers.
// Generators always pre-increment these.
static int nRegs = 0;
static int nLabs=0;

static int current_fn_name = 0;
static int deref_lvl = 0;

typedef int Register;
typedef int Label;

// generate code for the node. Return which reg number the result is stored in.
// returns 0 in case of void
// returns negative in case of failure
typedef const AST *const CA;

typedef int Generator(CA);

static Generator gen, genWhile, genConsts, genIf, genFn, genPgm, genAssgn, genIdx, genPrint, genScan, genIdxPtr, genLhs;

static FILE *out;
void codegen(CA a, FILE *o) {
    out = o;
    gen(a);
}

static char *ctype(CA a) {
    Type t= a->type;
    if (t.kind==karr){
        deref_lvl = 1;
        if (t.elem == kint) return "/*ctype*/ int *";
        if (t.elem == kreal) return "/*ctype*/ double *";
        printAST(a,0);
        return "/*ctype???*/ void*";
    }
    if (t.isRef){
        deref_lvl = 1;
        if (t.kind == kint) return "/*ctype*/ int *";
        if (t.kind == kreal) return "/*ctype*/ double *";
        printAST(a,0);
        return "/*ctype???*/ void*";
    }
    deref_lvl = 0;
    if (t.kind == kint) return "/*ctype*/ int";
    if (t.kind == kreal) return "/*ctype*/ double";
    printAST(a,0);
    return "/*ctype???*/ void";
}

static Register genBin(
    const char *const type,
    const char *const op,
    const AST *const a
) {
    Register dest = ++nRegs;
    Register lhs = gen(a->kids[0]);
    Register rhs = gen(a->kids[1]);
    if (strcmp(op, "<>") == 0){
        fprintf(out, "%1$s $%2$d = (%1$s)$%3$d != (%1$s)$%4$d; // binary op\n", type, dest, lhs, rhs);
    } else {
        fprintf(out, "%1$s $%2$d = (%1$s)$%3$d %4$s (%1$s)$%5$d; // binary op\n", type, dest, lhs, op, rhs);
    }
    // type $r = (type)$lhs op (type)$rhs;
    return dest;
}

static int genScan(CA a) {
    size_t nArgs = a->nKids;
    // reify readln args
    Register regs[nArgs];
    char *formats[nArgs];
    for (size_t i=0; i<nArgs; i++){
        regs[i] = genLhs(a->kids[i]);
        formats[i] = a->kids[i]->type.kind==kint?" %%d":" %%lf";
    }
    // generate scanf
    fprintf(out, "(void)scanf(\"");
    for (size_t i=0; i<nArgs; i++) fprintf(out, formats[i]);
    fprintf(out, "\",");
    for (size_t i=0; i<nArgs; i++) fprintf(out, "$%d%s", regs[i], nArgs-1==i?");\n":",");
    return 0;
}

static int genPrint(CA a) {
    size_t nArgs = a->nKids;
    // reify readln args
    Register regs[nArgs];
    char *formats[nArgs];
    for (size_t i=0; i<nArgs; i++){
        regs[i] = gen(a->kids[i]);
        formats[i] = a->kids[i]->type.kind==kint?"%d":"%f";
    }
    // generate scanf
    fprintf(out, "(void)printf(\"");
    for (size_t i=0; i<nArgs; i++) fprintf(out, "%s%s", formats[i], nArgs-1==i?"\\n":" ");
    fprintf(out, "\",");
    for (size_t i=0; i<nArgs; i++) fprintf(out, "$%d%s", regs[i], nArgs-1==i?");\n":",");
    return 0;
}

static int genConsts(CA a) {
    for(size_t i=0; i<a->nKids; i++) {
        CA cons = a->kids[i];
        char *name = getString(cons->kids[0]->str);
        CA val = cons->kids[1];
        if (val->type.kind==kint) {
            fprintf(out, "const int $%s = %li;\n", name, val->ival);
        } else {
            fprintf(out, "const double $%s = %f;\n", name, val->dval);
        }
    }
    return 0;
}

static int genPgm(CA a) {
    fprintf(out, "#include <stdio.h>\n");
    fprintf(out, "#include <stdlib.h>\n");
    fprintf(out, "#include <string.h>\n");
    CA consts = a->kids[1];
    CA vars   = a->kids[2];
    CA subs   = a->kids[3];
    CA stmts  = a->kids[4];
    gen(consts);
    gen(vars);
    gen(subs);
    fprintf(out, "\n\nint main(void){\n");
    gen(stmts);
    fprintf(out, "}\n");
    return 0;
}

static int genVar(CA a) {
    assert(strcmp(a->kind, "ident")==0);
    char *name = getString(a->str);

    Type t = a->type;
    switch(t.kind) {
        case kint:
            fprintf(out, "int $%s; // int var\n", name);
            return 0;
        case kreal:
            fprintf(out, "double $%s; // real var\n", name);
            return 0;
        case karr: {
            int range = t.upb - t.lwb;
            char *ek = t.elem==kint?"int":"double";
            fprintf(out, "%s $%s[%d]; // array var\n", ek, name, range);
            return 0;
        }
        default: crash("bad variable decl");
    }
    return 0;
}

static int genVars(CA a) {
    for (size_t i=0; i<a->nKids; i++) {
        genVar(a->kids[i]);
    }
    return 0;
}

static int genSubs(CA a) {
    for (size_t i=0; i<a->nKids; i++) {
        genFn(a->kids[i]);
    }
    return 0;
}

static int genCall(CA a) {

    fprintf(out, "// Call site ###\n");

    Type fnType = a->kids[0]->type;
    const size_t nArgs = a->kids[1]->nKids;
    Register args[nArgs];
    // reify arguments
    for(size_t i=0; i<nArgs; i++) {
        CA arg = a->kids[1]->kids[i];
        if (fnType.argTypes[i].isRef){
            fprintf(out, "// begin reify VAR arg %lu\n", i);
            args[i] = genLhs(arg);
            // TODO if var arry, then deref 1nce
            // TODO slices
        } else {
            fprintf(out, "// begin reify VALUE arg %lu\n", i);
            args[i] = gen(arg);
        }
        fprintf(out, "// end reify arg %lu\n", i);
    }
    // print call
    Register r = 0;
    if (a->type.kind != kvoid){
        r=++nRegs;
        fprintf(out, "%s $%d = ", ctype(a), r);
    }
    fprintf(out, "$fn_%s(", getString(a->kids[0]->str));
    for(size_t i=0; i<nArgs; i++) {
        fprintf(out, "$%d%s", args[i], nArgs-1==i?");\n":",");
    }
    return r;
}

static int gen(CA a) {
    if (!a) return 0;
    const char *const kind = a->kind;

    if(a->nKids==2 && strstr("+-/*<>==<=", kind)) {return genBin("double", kind, a);}
    #define WITH(x) if (strcmp(kind, x)==0)
    WITH ("If")       {return genIf(a);}
    WITH ("Mod")      {return genBin("int", "%", a);}
    WITH ("Div")      {return genBin("int", "/", a);}
    WITH ("Or")       {return genBin("int", "||", a);}
    WITH ("And")      {return genBin("int", "&&", a);}
    WITH ("Not")      {
        Register r = ++nRegs;
        fprintf(out, "int $%d = !$%d; // int lit\n", r, gen(a->kids[0]));
        return r;
    }
    WITH ("Writeln")  {return genPrint(a);}
    WITH ("Readln")   {return genScan(a);}
    WITH ("Program")         {return genPgm(a);}
    WITH ("ConstDeclList")   {return genConsts(a);}
    WITH ("VarDeclList")     {return genVars(a);}
    WITH ("SubProgDeclList") {return genSubs(a);}
    WITH ("Assign")   {return genAssgn(a);}
    WITH ("Call")     {return genCall(a);}
    WITH ("While")    {return genWhile(a);}
    WITH ("StmtList") {for(size_t i=0; i<a->nKids; i++) gen(a->kids[i]);  return 0;}
    WITH ("intlit")   {
        Register r = ++nRegs;
        fprintf(out, "int $%d = %ld; // int lit\n", r, a->ival);
        return r;
    }
    WITH ("reallit")  {
        Register r = ++nRegs;
        fprintf(out, "double $%d = %f; // real lit\n", r, a->dval);
        return r;
    }
    WITH ("Index")    {
        Register ptr = genIdxPtr(a);
        Register r = ++nRegs;
        fprintf(out, "%s $%d = *$%d; // a[i]\n", ctype(a), r, ptr);
        return r;
    }
    WITH ("ident")    {
        Register r = ++nRegs;
        fprintf(out, "%s $%d = $%s; // gen -> rule ident\n", ctype(a), r, getString(a->str));
        return r;
    }
    WITH ("retval")   {
        Register r = ++nRegs;
        fprintf(out, "%s $%d = $ret; // retval RHS\n", ctype(a), r);
        return r;
    }

    crash(fmt("Could not codegen '%s' node\n",a->kind));

    return 0;
}

/////////////////////////
// HELPERS

static void genLabel(Label l){
    fprintf(out, "L%d:;\n", l);
}

static void genJmp(Label l){
    fprintf(out, "goto L%d;\n", l);
}

static void genJumpIfFalse(Register cond, Label l){
    fprintf(out, "if (!$%d) goto L%d; // goto else if not guard\n", cond, l);
}

/////////////////////////
// GENERATORS

static
int genWhile(CA a){
    // WHILE COND DO STMT
    // start: if (!cond) goto end; stmt; goto start; end:;

    Label start = ++nLabs;
    Label end = ++nLabs;

    fprintf(out, "// while loop (start=L%d, end=L%d)\n", start, end);
    genLabel(start);
    fprintf(out, "// while guard start\n");
    Register cond = gen(a->kids[0]);
    fprintf(out, "// while guard end\n");
    assert(cond > 0);

    genJumpIfFalse(cond, end);

    fprintf(out, "// while body start\n");
    assert(gen(a->kids[1]) == 0);
    fprintf(out, "// while body end\n");
    genJmp(start);
    genLabel(end);

    return 0;
}

static
int genIf(CA a) {
    // IF COND THEN X ELSE Y
    /*
     * if(!cond) goto elselab;
     * X
     * goto endlab;
     * elselab:;
     * Y
     * endlab:;
     */
    Label l_else = ++nLabs;
    Label l_end = ++nLabs;
    Register cond = gen(a->kids[0]);
    genJumpIfFalse(cond, l_else);
    gen(a->kids[1]); // cond true
    genJmp(l_end);
    genLabel(l_else);
    gen(a->kids[2]); // cond false
    genLabel(l_end);
    return 0;
}


static void genParams(CA a) {
    for(size_t i=0; i<a->nKids; i++) {
        CA arg = a->kids[i];
        fprintf(out, "%s $%s%c", ctype(arg), getString(arg->str), a->nKids-1==i?')':',');
    }
}

static void genArrCopies(CA a){
    // generate array copy code
    // if the argument is an array and not var
    size_t nArgs = a->nKids;
    for (size_t i=0; i<nArgs; i++){
        CA arg = a->kids[i];
        Type t = arg->type;
        if (t.kind == karr && !t.isRef){
            // tmp = arr
            Register tmp_arr = ++nRegs;
            char *argname = getString(arg->str);
            fprintf(out, "%s *$%d = $%s;\n", t.elem==kint?"int":"double", tmp_arr, argname);
            // arr = malloc(range*size);
            size_t range = (t.upb-t.lwb);
            size_t elemsize = (t.elem==kint?4:8);
            size_t totalSize = range*elemsize;
            fprintf(out, "$%s = malloc(%lu); // %lu * %lu\n", argname, totalSize, range, elemsize);
            // memcpy(arr, tmp, size*range)
            fprintf(out, "memcpy($%s, $%d, %lu);\n", argname, tmp_arr, totalSize);
        }
    }
}


static void genArrFrees(CA a){
    // generate array copy code
    // if the argument is an array and not var
    size_t nArgs = a->nKids;
    for (size_t i=0; i<nArgs; i++){
        CA arg = a->kids[i];
        Type t = arg->type;
        if (t.kind == karr && !t.isRef){
            char *argname = getString(arg->str);
            fprintf(out, "free($%s);\n", argname);
        }
    }
}

static
int genFn(CA a) {
    // arrays by var: pointer, don't copy
    // arrays by copy: tmp_pointer, memcpy
    // anything else by var: pointer

    CA id = a->kids[0];
    current_fn_name = id->str;
    char *name = getString(current_fn_name);
    Type t = id->type;

    // what the fuck
    // ReSharper disable once CppDFAUnreachableCode
    int isProc = t.retn==kvoid;
    const char *rettype = isProc?"void":(t.retn == kint?"int":"double");
    fprintf(out,"\n// Function declaration\n%s $fn_%s(", rettype, name);

    genParams(a->kids[1]);

    fprintf(out,"{\n");
    // generate return value register
    if (!isProc) fprintf(out,"%s $ret;\n", rettype);
    genArrCopies(a->kids[1]);

    genVars(a->kids[2]);
    gen(a->kids[3]);

    genArrFrees(a->kids[1]);

    if (!isProc) fprintf(out,"return $ret;\n");
    fprintf(out,"}\n\n");

    return 0;
}

static int genIdxPtr(CA a) {
    // arr[i], where arr[lwb..upb]
    // int $1 = i;
    // int $2 = {arr.lwb};
    // int $3 = $2 - $3;
    // CTYPE $4 = arr;
    // CTYPE $5 = $4 + $3;
    Register r_i = gen(a->kids[1]);
    Register r_lwb = ++nRegs;
    Register r_actual_i = ++nRegs;
    fprintf(out, "int $%d = %d; // array lower bound\n", r_lwb, a->kids[0]->type.lwb);
    fprintf(out, "int $%d = $%d - $%d; // array index\n", r_actual_i, r_i, r_lwb);
    Register r_arr = ++nRegs;
    Register r_res = ++nRegs;
    char *t_arr = ctype(a);
    char *v_arr = getString(a->kids[0]->str);
    fprintf(out, "%s *$%d = $%s;\n", t_arr, r_arr, v_arr);
    fprintf(out, "%s *$%d = $%d+$%d; // L-value arr[i] = arr+(i-lwb)\n", t_arr, r_res, r_arr, r_actual_i);
    deref_lvl = 1;
    return r_res;
}

static int genLhs(CA a) {
    // generate an lvalue (pointer)
    if (0==strcmp(a->kind, "Index")) {
        // if a is arr[i], return arr+i
        Register r = genIdxPtr(a);
        deref_lvl =1;
        return r;
    } else if(0==strcmp(a->kind, "ident")) {
        // if a is a variable, return &a
        Register r = ++nRegs;
        fprintf(out, "%s *$%d = &$%s; // LHS is ident\n", ctype(a), r ,getString(a->str));
        deref_lvl++;
        return r;
    } else if(0==strcmp(a->kind, "retval")) {
        Register r = ++nRegs;
        fprintf(out, "%s *$%d = &$ret; // retval LHS\n", ctype(a), r);
        deref_lvl = 1;
        return r;
    }
    crash(fmt("Attempt to generate L-value for '%s'", a->kind));
}

static
int genAssgn(CA a) {
    Register rhs = gen(a->kids[1]);
    if (0==strcmp(a->kids[0]->kind, "ident") && a->kids[0]->str == current_fn_name){
        fprintf(out, "$ret = $%d; // return assignment\n", rhs);
        return 0;
    } else {
        Register lhs = genLhs(a->kids[0]); // the register is always a pointer type
        if (deref_lvl == 0){
            crash("deref lvl of lvalue can not be 0? wtfbro'");
        }
        if (deref_lvl == 1){
            fprintf(out, "*$%d = $%d; // assign L-value \n", lhs, rhs);
        } else {
            fprintf(out, "**$%d = $%d; // assign L-value of VAR arg (ugh) \n", lhs, rhs);
        }
        return 0;
    }
}