%{

  #include "prelude.h"

  void yyerror(char *msg);    /* forward declaration */
  /* exported by the lexer (made with flex) */
  extern int yylex(void);
  extern char *yytext;
  extern void showErrorLine();
  extern void initLexer(FILE *f);
  extern void finalizeLexer();

AST *ast = NULL;

%}

%token PROGRAM CONST IDENTIFIER READLN WRITELN VAR ARRAY RANGE INTNUMBER REALNUMBER OF
       FUNCTION PROCEDURE BEGINTOK ENDTOK ASSIGN IF THEN ELSE WHILE DO
       RELOPLT RELOPLEQ RELOPEQ RELOPNEQ RELOPGEQ RELOPGT INTEGER REAL
       AND OR NOT DIV MOD SKIP

%left '+' '-'
%left '*' '/' DIV MOD
%left OR
%left AND
%left NOT

%union {
  int      ival;       /* used for passing int values from lexer to parser */
  float    dval;       /* used for passing double values from lexer to parser */
  unsigned string;     /* string table entry */
  struct AST *ast;     /* parsers return what they parsed */
  const char *charptr; /* because I am too lazy for arith exprs */
}

%type <ast> ConstDecl NumericValue VarDecl IdentifierList TypeSpec BasicType FuncProcDecl SubProgDecl SubProgHeading PossibleParameters Parameters ParameterList ParamList CompoundStatement OptionalStatements StatementList Statement LhsList Lhs BoolAtom ArithExprList ArithExpr ProcedureCall
%type <ast> Guard
%type <ival> INTNUMBER
%type <dval> REALNUMBER
%type <string> IDENTIFIER
%type <charptr> Relop

%%

program            : PROGRAM IDENTIFIER ';'
                     ConstDecl
                     VarDecl
                     FuncProcDecl
                     CompoundStatement
                     '.' {ast = newAST("Program", 5, newIdent($2), $4, $5, $6, $7);}
                   ;

ConstDecl          : ConstDecl CONST IDENTIFIER RELOPEQ NumericValue ';' {$$ = addKid($1, newAST("ConstDecl", 2, newIdent($3), $5));}
                   | %empty {$$ = newAST("ConstDeclList", 0);}
                   ;

NumericValue       : INTNUMBER {$$ = newIntLit($1);}
                   | REALNUMBER {$$ = newRealLit($1);}
                   ;

VarDecl            : VarDecl VAR IdentifierList ':' TypeSpec ';' {
                            AST *idents = $3;
                            Type type = $5->type; free($5);
                            for (size_t i=0; i<idents->nKids; i++){
                                AST *ident = idents->kids[i];
                                ident->type = type;
                                $$ = addKid($$, ident);
                            }
                            free(idents);
                        }
                   | %empty {$$ = newAST("VarDeclList", 0);}
                   ;

IdentifierList     : IDENTIFIER {$$ = newAST("IdentList", 1, newIdent($1));}
                   | IdentifierList ',' IDENTIFIER {$$ = addKid($1, newIdent($3));}
                   ;

TypeSpec           : BasicType {$$ = $1;}
                   | ARRAY '[' INTNUMBER RANGE INTNUMBER ']' OF BasicType {Kind k = ($8)->type.kind; i64 lo = $3; i64 hi = $5; $$ = newTypeAST(newType_arr(k, lo, hi)); free($8);}
                   ;

BasicType          : INTEGER {$$ = newTypeAST(newType_int(0,0));}
                   | REAL {$$ = newTypeAST(newType_real(0,0));}
                   ;

FuncProcDecl       : FuncProcDecl SubProgDecl ';' {$$ = addKid($1, $2);}
                   | %empty {$$ = newAST("SubProgDeclList",0);}
                   ;

SubProgDecl        : SubProgHeading VarDecl CompoundStatement {$$ = usurpKids($1, newAST("SubProgDecl", 2, $2, $3));}
                   ;

SubProgHeading     : FUNCTION IDENTIFIER Parameters ':' BasicType ';' {$$ = newAST("Function", 2, newIdent($2), $3); $$->type=$5->type;free($5);}
                   | PROCEDURE IDENTIFIER PossibleParameters ';' {$$ = newAST("Procedure", 2, newIdent($2), $3); $$->type.kind = kvoid;}
                   ;

PossibleParameters : Parameters {$$ = $1;}
                   | %empty {$$ = NULL;}
                   ;

Parameters         : '(' ParameterList ')' {$$ = $2;}
                   ;

ParameterList      : ParamList {$$ = usurpKids(newAST("ParameterList", 0), $1);}
                   | ParameterList ';' ParamList {$$ = usurpKids($1, $3);}
                   ;

ParamList          : VAR IdentifierList ':' TypeSpec {
                            Type t = ($4)->type;
                            t.isRef = 1;
                            $$ = $2;
                            $$->type = t;
                            cascadeType($$);
                            free($4);
                        }
                   | IdentifierList ':' TypeSpec {
                           Type t = ($3)->type;
                           $$ = $1;
                           $$->type = t;
                           cascadeType($$);
                           free($3);
                       }
                   ;

CompoundStatement  : BEGINTOK OptionalStatements ENDTOK {$$ = $2;}
                   ;

OptionalStatements : StatementList {$$ = $1;}
                   | %empty {$$ = NULL;}
                   ;

StatementList      : Statement {$$ = newAST("StmtList", 1, $1);}
                   | StatementList ';' Statement {$$ = addKid($1, $3);}
                   ;

Statement          : Lhs ASSIGN ArithExpr {$$ = newAST("Assign", 2, $1, $3);}
                   | SKIP {$$ = newTerm("Skip");}
                   | ProcedureCall {$$ = $1;}
                   | CompoundStatement {$$ = $1;}
                   | IF Guard THEN Statement ELSE Statement {$$ = newAST("If", 3, $2, $4, $6);}
                   | WHILE Guard DO Statement {$$ = newAST("While", 2, $2, $4);}
                   ;

LhsList            : Lhs {$$ = newAST("LhsList", 1, $1);}
                   | LhsList ',' Lhs {$$ = addKid($1, $3);}

Lhs                : IDENTIFIER {$$ = newIdent($1);}
                   | IDENTIFIER '[' ArithExpr ']' {AST *id = newIdent($1); $$ = newAST("Index", 2, id, $3);}
                   ;

ProcedureCall      : IDENTIFIER {$$ = newAST("Call", 1, newIdent($1));}
                   | IDENTIFIER '(' ArithExprList ')' {$$ = newAST("Call", 2, newIdent($1), $3);}
                   | READLN '(' LhsList ')' {$$ = usurpKids(newAST("Readln", 0), $3);}
                   | WRITELN '(' ArithExprList ')' {$$ = usurpKids(newAST("Writeln", 0), $3);}
                   ;

Guard              : BoolAtom {$$ = $1;}
                   | NOT Guard {$$ = newUnop("Not", $2);}
                   | Guard OR Guard {$$ = newBinop("Or", $1, $3);}
                   | Guard AND Guard {$$ = newBinop("And", $1, $3);}
                   | '(' Guard ')' {$$ = $2;}
                   ;

BoolAtom           : ArithExpr Relop ArithExpr {$$ = newBinop($2, $1, $3);}
                   ;

Relop              : RELOPLT {$$ = "<";}
                   | RELOPLEQ {$$ = "<=";}
                   | RELOPEQ {$$ = "==";}
                   | RELOPNEQ {$$ = "<>";}
                   | RELOPGEQ {$$ = ">=";}
                   | RELOPGT {$$ = ">";}
                   ;

ArithExprList      : ArithExpr {$$ = newAST("ArithExprList", 1, $1);}
                   | ArithExprList ',' ArithExpr {$$ = addKid($1, $3);}
                   ;

ArithExpr          : IDENTIFIER {$$ = newIdent($1);}
                   | IDENTIFIER '[' ArithExpr ']' {$$ = newBinop("Index", newIdent($1), $3);}
                   | IDENTIFIER '[' ArithExpr RANGE ArithExpr ']' {$$ = newAST("Slice", 3, newIdent($1), $3, $5);}
                   | IDENTIFIER '(' ArithExprList ')' {$$ = newBinop("Call", newIdent($1), $3);}
                   | INTNUMBER {$$ = newIntLit($1);}
                   | REALNUMBER {$$ = newRealLit($1);}
                   | ArithExpr '+' ArithExpr {$$ = newBinop("+", $1, $3);}
                   | ArithExpr '-' ArithExpr {$$ = newBinop("-", $1, $3);}
                   | ArithExpr '*' ArithExpr {$$ = newBinop("*", $1, $3);}
                   | ArithExpr '/' ArithExpr {$$ = newBinop("/", $1, $3);}
                   | ArithExpr DIV ArithExpr {$$ = newBinop("Div", $1, $3);}
                   | ArithExpr MOD ArithExpr {$$ = newBinop("Mod", $1, $3);}
                   | '-' ArithExpr {$$ = newUnop("Neg", $2);}
                   | '(' ArithExpr ')' {$$ = $2;}
                   ;

%%

void yyerror (char *msg) {
  showErrorLine();
  fprintf(stderr, "%s (detected at token=", msg);
  printToken(yychar, stderr);
  fprintf(stderr, ").\n");

  printf("ERRORS: 1\nWARNINGS: 0\nREJECTED\n");
  exit(EXIT_SUCCESS);  // EXIT_SUCCESS because we use Themis
}

int main(int argc, char *argv[]) {
  if (argc != 3) {
    fprintf(stderr, "Usage: %s [pasfile] [outfile]\n", argv[0]);
    return EXIT_FAILURE;
  }
  
  FILE *input = (strcmp(argv[1], "-") == 0) ? stdin : fopen(argv[1], "r");
  if(input == NULL) {
    fprintf(stderr, "Failed to open input file!\n");
    exit(EXIT_FAILURE);
  }

  FILE *output = (strcmp(argv[2], "-") == 0) ? stdout : fopen(argv[2], "w");
  if(output == NULL) {
    fprintf(stderr, "Failed to open output file!\n");
    exit(EXIT_FAILURE);
  }

  addFormatters();
  initStrings();
  initLexer(input);
  yyparse();

  if(!ast) crash("Could not generate AST");

  analyze(ast);
  diagnosis();
  codegen(ast, output);
  freeAllBSTs();
  freeAST(ast);
  finalizeLexer();

  fclose(input);
  fclose(output);

  return EXIT_SUCCESS;
}
