#include "prelude.h"

void crash(char *why){
	fprintf(stderr, "Internal compiler error: %s\n", why);
	if (__builtin_constant_p(why) == 0) {
		free(why);
	}
    exit(EXIT_FAILURE);
}
