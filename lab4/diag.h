#pragma once

#include "prelude.h"

void addWarn(char *msg, int line, int col);
void addErr(char *msg, int line, int col);

// print all warns and errs on stderr
// and print a summary on stdout
void diagnosis(void);
