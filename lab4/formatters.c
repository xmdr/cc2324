#include "prelude.h"

void fmtSize(size_t *nbytes, char **dst, size_t *n) {
    const char* suffixes[] = {"B", "kB", "mB", "gB", "tB"};
    u32 suffixIndex = 0;
    u32 size = *nbytes;

    while (size >= 1024 && suffixIndex < sizeof(suffixes) / sizeof(suffixes[0]) - 1) {
        size /= 1024;
        suffixIndex++;
    }

    *n = asprintf(dst, "%.3g%s", (double)size, suffixes[suffixIndex]);
}

void fmtOrd(size_t *no, char **dst, size_t *n) {
    size_t x = 1 + *no;
    size_t tens = (x/10)%10;
    size_t units = x%10;
    char *suffix = "th";
    if (tens != 1) {
        if (units == 1) suffix = "st";
        if (units == 2) suffix = "nd";
        if (units == 3) suffix = "rd";
    }
    *n = asprintf(dst, "%zu%s", x, suffix);
}

void fmtSym(u32 *sym, char **dst, size_t *n){
    *n = asprintf(dst, "'%s'", getString(*sym));
}

void fmtLoc(Location *loc, char **dst, size_t *n){
    *n = asprintf(dst, "linw %d (column %d): ", loc->line, loc->col);
}

void addFormatters(void) {
    addFormat("sym",   (Formatter)fmtSym);
    addFormat("loc",   (Formatter)fmtLoc);
    addFormat("ord",   (Formatter)fmtOrd);
    addFormat("size",  (Formatter)fmtSize);
    addFormat("type",  (Formatter)fmtType);
}

static
int testFormatters() {
    u32 id = getId(sizeof "hello", "hello");
    Range range = {12, 30};

    size_t total = 99999999999999999;
    size_t free = 1>>7;

    char *p = fmt("%{sym} can not be assigned %{range} %{ord}", &id, &range, &(size_t){20});
    addWarn(p, 420, 60);
    addErr(fmt("Total RAM: %{size}, Free RAM: %{size}", &total, &free), 1, 1);
    freeStrings();

    diagnosis();

    return 0;
}
