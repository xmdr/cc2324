#include "prelude.h"

// global scope
static BST *global = NULL;

static BST *bst_to_be_freed[999];
static size_t fbst = 0;

// the current scope that is targeted for shadowing anal
static BST **current = &global;

static u32 currfn = 0; // track the function name we are currently "in". This is used to change "ident" nodes into "retval" nodes.
static u32 isForCall = 0;

static inline
SymInfo *getSym(u32 symbol, int line, int col, jmp_buf notFound) {
    SymInfo *found = BST_get(*current, symbol);

    // If not found in local scope, search global scope
    if (!found && *current != global) {
        found = BST_get(global, symbol);
    }

    if (!found){
        addErr(fmt("Variable %{sym} is undeclared", &symbol), line, col);
        longjmp(notFound,1);
    }

    assert(found != NULL);
    return found;
}

// All analyzer functions perform all checks to ensure the specific kind of AST node is valid
// It also reifies the type of the AST node.
typedef void Analyzer(AST *);

Analyzer aCompound, aStmt, aWhile, aGuard, aArithExpr; // forward decl; fuck C

#define eachkid(parent, kid) \
AST *kid; for (size_t _kidIndex = 0; _kidIndex < (parent)->nKids && (_kidIndex, kid = (parent)->kids[_kidIndex]); ++_kidIndex)

// this code is super duper stuper
void declare(u32 symbol, SymInfo *info) {
    jmp_buf duplicate_exception;
    if (setjmp(duplicate_exception) == 0) {
        *current = BST_set(*current, symbol, info, duplicate_exception);
    } else {
        addErr(fmt("Attempt to redeclare %{sym}", &symbol), info->decl.line, info->decl.col);
    }
}

// var decl list
void aVars(AST *a) {
    eachkid(a, decl) {
        u32 ident = decl->str;
        SymInfo *si = newSymInfo(decl->type, decl->line, decl->col);
        declare(ident, si);
    }
    a->type.kind = kvoid;
}

void aSkip(AST *skip){skip->type.kind = kvoid;}

void aId(AST *a) {
    if (0!=strcmp(a->kind, "ident")) crash("Analyzer attempt to aId non ident");

    jmp_buf notFound;
    if (setjmp(notFound)){
        a->type.kind = kinvalid;
        return;
    }

    u32 id = a->str;
    int line = a->line;
    int col = a->col;
    SymInfo *si = getSym(id, line, col, notFound);
    a->type = si->type;

    if (a->str == currfn && !isForCall){
        a->kind = "retval";
        a->type.kind = a->type.retn;
        a->type.isConst = 0;
    }
}

void aIndex(AST *a) {
    if (0!=strcmp(a->kind, "Index")) crash("Analyzer attempt to aIndex non Index");
    AST *arr = a->kids[0];
    aId(arr);
    int line = arr->line;
    int col = arr->col;
    if (arr->type.kind != karr) {
        addErr(fmt("Variable %{sym} of %{type} can not be indexed", &arr->str, &arr->type), line, col);
    }
    aArithExpr(a->kids[1]);
    a->type.kind = arr->type.elem;
}

void aComptTimeInt(AST *a) {
    aArithExpr(a);
    if (a->type.kind == kinvalid) return;

    int line = a->line;
    int col = a->col;

    if (strcmp(a->kind, "intlit") == 0) {
        return;
    }
    if (strcmp(a->kind, "ident") == 0) {
        jmp_buf willNeverHappen;
        if (setjmp(willNeverHappen)) crash("Somehow a symbol passed the first lookup but failed the second");
        SymInfo *s = getSym(a->str, line, col, willNeverHappen);
        if (s->type.isConst) {
            a->type = s->type;
            a->ival = s->ival;
            return;
        }
    }
    addErr(fmt("Array slice ranges need to be CONST identifiers or integer literals"), line, col);
    a->type.kind = kinvalid;
}

void aSlice(AST *a) {
    if (0!=strcmp(a->kind, "Slice")) crash("Analyzer attempt to aSlice non Slice");
    assert(a->nKids == 3);
    AST *arr = a->kids[0];
    aId(arr);
    int line = arr->line;
    int col = arr->col;
    if (arr->type.kind != karr) {
        addErr(fmt("Variable %{sym} of %{type} can not be sliced", &arr->str, &arr->type), line, col);
        a->type.kind = kinvalid;
        return;
    }

    // check that the sliced range is a compile time constant
    AST *lwbn = a->kids[1];
    AST *upbn = a->kids[2];
    aComptTimeInt(lwbn);
    aComptTimeInt(upbn);

    if (lwbn->type.kind == kinvalid || upbn->type.kind == kinvalid) {
        a->type.kind = kinvalid;
        return;
    }

    int range = upbn->ival - lwbn->ival;
    if (range < 1) {
        addErr(fmt("[%d..%d] is not a valid slice range", lwbn->ival, upbn->ival), line, col);
        a->type.kind = kinvalid;
        return;
    }

    a->type = arr->type;
    a->type.lwb = lwbn->ival;
    a->type.upb = upbn->ival;
}

// lhs of assignment
void aLhs(AST *a) {
    // lhs may be an id or id[Arithexpr]
    AST *idAst;
    if (0==strcmp(a->kind, "Index")) {
        aIndex(a);
        idAst = a->kids[0];
    } else if (0==strcmp(a->kind, "ident")){
        idAst = a;
        aId(a);
    } else {
        crash("Analyzer encountered invalid left hand side AST");
        __builtin_unreachable();
    }

    if (idAst->type.kind == kinvalid) {
        a->type.kind = kinvalid;
        return;
    }

    jmp_buf notFound;
    if (setjmp(notFound)){
        idAst->type.kind = kinvalid;
        a->type.kind = kinvalid;
        return;
    }

    SymInfo *s = getSym(idAst->str, idAst->line, idAst->col, notFound);
    if (!s->firstWrite){
        s->firstWrite = a->line;
    }
}

void aCall(AST *a) {
    if (0!=strcmp(a->kind, "Call")) crash("Analyzer attempt to aCall non call");

    isForCall = 1;

    jmp_buf notFound;
    if (setjmp(notFound)){
        a->type.kind = kinvalid;
        return;
    }

    // check args
    AST *fn = a->kids[0];
    aId(fn);
    if (fn->type.kind == kinvalid) {
        a->type.kind = kinvalid;
        return;
    }
    int line = fn->line, col = fn->col;
    SymInfo *fnSi = getSym(fn->str, line, col, notFound);
    Type fnType = fnSi->type;

    if (fnType.nArgs > 0) {
        AST *args = a->kids[1];

        if (fnType.nArgs != args->nKids) {
            char *msg = fmt(
                "Expected %d arguments, got %d when calling %{sym} (%{type})",
                                fnType.nArgs,     args->nKids,   &fn->str, &fnType
            );
            addErr(msg, line, col);
        }

        for (size_t i=0; i<fnType.nArgs; i++) {
            AST *arg = args->kids[i];
            aArithExpr(arg);
            Type expected = arg->type;
            Type given    = fnType.argTypes[i];
            char *err=NULL, *warn=NULL;
            typeCheck(given, expected, &warn, &err);
            if (warn) {
                addWarn(fmt("Type warning with %{ord} argument of %{sym}: %s",&i, &fn->str, warn), line, col);
                free(warn);
            }
            if (err) {
                addErr(fmt("Type error with %{ord} argument of %{sym}: %s",&i, &fn->str, err), line, col);
                free(err);
            }

            if (fnSi->argprops[i].overwritten && given.isConst) {
                addErr(fmt("The %{ord} argument can not be const because %{sym} uses it as output parameter", &i, &fn->str), line, col);
            }

            if (fnSi->argprops[i].readfirst && 0) {
                // TODO
                char *msg = fmt("The %{ord} argument has to be initialized because %{sym} uses it as input parameter");
                addErr(msg, line, col);
            }
        }
        args->type.kind = kvoid;
    }

    a->type.kind    = fnType.retn;
    a->type.isConst = 1;

    isForCall = 0;
}

void aIntLit(AST *a) {
    if (0!=strcmp(a->kind, "intlit")) crash("Analyzer attempt to aIntLit non intlit");
    // always good
}

void aRealLit(AST *a) {
    if (0!=strcmp(a->kind, "reallit")) crash("Analyzer attempt to aRealLit non reallit");
    // always good
}

void aBinA(AST *a) {
    // binary arithmetic expression
    AST *lhs = a->kids[0];
    AST *rhs = a->kids[1];
    aArithExpr(lhs);
    aArithExpr(rhs);

    int line = lhs->line;
    int col  = lhs->col;

    // L/R can be int/int, int/real, real/int, real/real
    // ints get promoted to real when one operand is real
    Type l = lhs->type;
    Type r = lhs->type;

    int lint  = l.kind==kint;
    int lreal = l.kind==kreal;
    int rint  = r.kind==kint;
    int rreal = r.kind==kreal;
    switch( (lint<<3) | (lreal<<2) | (rint<<1) | (rreal) ){
        case 0b1010: // int/int
            a->type.kind = kint;
            break;

        case 0b1001: // int/real
        case 0b0110: // real/int
        case 0b0101: // real/real
            a->type.kind = kreal;
            break;

        default: // uh oh
            addErr(fmt("Attempt to perform %{type} %s %{type} (only reals or integers allowed)", &l, a->kind, &r), line, col);
            a->type.kind = kinvalid;
    }

    a->type.isConst = 1;
}

void aBinInt(AST *a){
    AST *lhs = a->kids[0];
    AST *rhs = a->kids[1];
    aArithExpr(lhs);
    aArithExpr(rhs);
    int line = lhs->line;
    int col = lhs->col;
    Type l = lhs->type;
    Type r = lhs->type;
    if (l.kind != kint || r.kind != kint){
        addErr(fmt("Attempt to perform %{type} %s %{type} (only integers allowed)", &l, a->kind, &r), line, col);
        a->type.kind = kinvalid;
    } else {
        a->type.kind = kint;
    }
    a->type.isConst = 1;
}

void aNeg(AST *a){
    aArithExpr(a->kids[0]);
    a->type = a->kids[0]->type;
}

void aArithExpr(AST *a) {
    struct {char *match; Analyzer *rule;} lut[] = {
        {"ident", aId},
        {"intlit", aIntLit},
        {"reallit", aRealLit},
        {"Index", aIndex},
        {"Slice", aSlice},
        {"Call", aCall},
        {"+", aBinA},
        {"-", aBinA},
        {"/", aBinA},
        {"*", aBinA},
        {"Div", aBinInt},
        {"Mod", aBinInt},
        {"Neg", aNeg},
    };
    const size_t lutlen = (sizeof lut) / (sizeof *lut);
    for (size_t i=0; i<lutlen; i++) {
        if (0==strcmp(lut[i].match, a->kind)) {
            lut[i].rule(a);
            return;
        }
    }
    addErr(fmt("Invalid ArithExpr: '%s'", a->kind), a->line, a->col);
    a->type.kind = kinvalid;
}

void aAssign(AST *a) {
    AST *lhs = a->kids[0];
    AST *rhs = a->kids[1];
    int line = lhs->line;
    int col = lhs->col;
    a->type.kind = kvoid;

    aLhs(lhs);
    aArithExpr(rhs);

    // type check
    Type expected = lhs->type;
    Type t = rhs->type;
    if (expected.kind == kinvalid || t.kind == kinvalid) {
        a->type.kind = kinvalid;
        return;
    }

    // check if assignee is not array
    if (expected.kind == karr) {
        addErr(fmt("Minipas does not support assigning to entire arrays at once"),line,col);
        a->type.kind = kinvalid;
        return;
    }

    // check if assignee is not const
    if (expected.isConst) {
        addErr(fmt("Attempt to modify a constant"),line,col);
        a->type.kind = kinvalid;
        return;
    }

    // assigning the function name to set a return value
    if (expected.kind == kfn){
        if (expected.retn == kvoid){
            addErr(fmt("Procedures do not return anything, therefore a return value can not be set"),line,col);
            a->type.kind = kinvalid;
            return;
        }
        expected.kind = expected.retn;
    }

    if (t.kind == kvoid) {
        addErr(fmt("Procedures do not return a value"),line,col);
        a->type.kind = kinvalid;
        return;
    }

    if (expected.kind == t.kind){
        return; // ok
    }

    if (expected.kind == kint){
        if (t.kind == kreal){
            addWarn(fmt("Truncation of REAL occurs when assigning to INTEGER"), line, col);
        } else {
            addErr(fmt("Attempt to assign %{type} to %{type}", &t, &expected), line, col);
            a->type.kind = kinvalid;
        }
    } else if (expected.kind == kreal){
        if (t.kind != kreal && t.kind != kint){
            addErr(fmt("Attempt to assign %{type} to %{type}", &t, &expected), line, col);
            a->type.kind = kinvalid;
        }
    } else {
        addErr(fmt("%{type} can not be assigned", &expected), line, col);
        a->type.kind = kinvalid;
    }
}

void aBinGuard(AST *a){
    aGuard(a->kids[0]);
    aGuard(a->kids[1]);
    int lhs = a->kids[0]->type.kind;
    int rhs = a->kids[1]->type.kind;
    if (lhs == kinvalid || rhs == kinvalid) {
        a->type.kind = kinvalid;
    } else {
        a->type.kind = kbool;
    }
    a->type.isConst = 1;
}

void aUnGuard(AST *a){
    aGuard(a->kids[0]);
    if (a->kids[0]->type.kind == kinvalid) {
        a->type.kind = kinvalid;
    } else {
        a->type.kind = kbool;
    }
    a->type.isConst = 1;
}

void aRelOp(AST *a){
    aBinA(a);
    if (a->type.kind == kundecided) {
        crash(fmt("Analyzer could not handle this relop '%s'", a->kind));
    }
    if (a->type.kind != kinvalid) {
        a->type.kind = kbool;
    }
}

void aGuard(AST *a){
    struct {char *match; Analyzer *rule;} lut[] = {
        {"Not", aUnGuard},
        {"Or", aBinGuard},
        {"And", aBinGuard},
        {">=", aRelOp},
        {"<=", aRelOp},
        {"<", aRelOp},
        {">", aRelOp},
        {"<>", aRelOp},
        {"==", aRelOp},
    };
    const size_t lutlen = (sizeof lut) / (sizeof *lut);
    for (size_t i=0; i<lutlen; i++) {
        if (0==strcmp(lut[i].match, a->kind)) {
            lut[i].rule(a);
            if (a->type.kind == kundecided) {
                goto err;
            }
            return;
        }
    }
    err:
    crash(fmt("Could not analyze guard '%s'", a->kind));
}

void aIf(AST *a) {
    if (0!=strcmp(a->kind, "If")) crash("Analyzer attempt to aIf non If");
    AST *cond = a->kids[0];
    AST *yes = a->kids[1];
    AST *no  = a->kids[2];
    aGuard(cond);
    aStmt(yes);
    aStmt(no);
    a->type.kind = kvoid;
}

void aReadln(AST *a) {
    a->type.kind = kvoid;
    eachkid(a, arg) {
        aLhs(arg);
        if (arg->type.kind == kinvalid) {
            a->type.kind = kinvalid;
        }
    }
}

void aWriteln(AST *a) {
    a->type.kind = kvoid;
    eachkid(a, arg) {
        aArithExpr(arg);
        if (arg->type.kind == kinvalid) {
            a->type.kind = kinvalid;
        }
    }
}

void aStmt(AST *a) {
    struct {char *match; Analyzer *rule;} lut[] = {
        {"Assign", aAssign},
        {"Call", aCall},
        {"StmtList", aCompound},
        {"If", aIf},
        {"Skip", aSkip},
        {"While", aWhile},
        {"Readln", aReadln},
        {"Writeln", aWriteln}
    };
    const size_t lutlen = (sizeof lut) / (sizeof *lut);
    for (size_t i=0; i<lutlen; i++) {
        if (0==strcmp(lut[i].match, a->kind)) {
            lut[i].rule(a);
            goto analyzed;
        }
    }
    crash(fmt("Analyzer did not recognize AST node '%s' on line %d as a valid Statement", a->kind, a->line));

    analyzed:
    if (a->type.kind != kvoid) {
        if (a->type.kind == kundecided) {
            crash(fmt("Semantic analysis went wrong, type for '%s' node could not be decided", a->kind));
        } else if (a->type.kind != kinvalid) {
            addWarn(fmt("%{type} was discarded", &a->type), a->line, a->col);
        }
    }
}

void aCompound(AST *a) {
    assert(a != NULL);
    eachkid(a, stmt) {
        aStmt(stmt);
    }
    a->type.kind = kvoid;
}

void aWhile(AST *a){
    AST *cond = a->kids[0];
    AST *body = a->kids[1];
    aGuard(cond);
    aStmt(body);
    a->type.kind = kvoid;
}

void declFn(AST *fn) {
    assert(fn->nKids == 4);
#ifdef DBG
    printf("%lu children\n", fn->nKids);
    for(size_t i=0; i<fn->nKids; i++) {
        printf("  %p\n", fn->kids[i]);
    }
#endif

    AST *params = fn->kids[1];
    size_t nArgs = params!=NULL ? params->nKids : 0;
    u32 fnName = fn->kids[0]->str;
    currfn = fnName;
    SymInfo *fnSi;

    /* declare function itself */ {
        Type t = {
            .kind = kfn,
            .retn = fn->type.kind,
            .nArgs = nArgs
        };

        fnSi = newSymInfo(t, fn->line, fn->col);
        fnSi->argprops = calloc(nArgs, sizeof *fnSi->argprops);
        t.argTypes = calloc(nArgs, sizeof *t.argTypes);

        for(size_t i=0; i<nArgs; i++) {
            fnSi->argprops[i].name = params->kids[i]->str;
            fnSi->argprops[i].readfirst = 0;
            fnSi->argprops[i].overwritten = 0;
            t.argTypes[i] = params->kids[i]->type;
        }
        fnSi->firstWrite = 0;
        fnSi->firstRead = 0;
        assert(fnSi->argprops != NULL);
        fnSi->type = t;        // set it again, this time `t` has the args
        declare(fnName, fnSi); // declare function in global scope
        fn->kids[0]->type = t;
    }


    // enter new local scope
    BST *local = NULL;
    current = &local;

    /* declare arguments */
    for(size_t i=0; i<nArgs; i++) {
        SymInfo *argInfo = newSymInfo(params->kids[i]->type, params->kids[i]->line, params->kids[i]->col);
        argInfo->isArg = 1;
        declare(params->kids[i]->str, argInfo);
    }
    if (params) {
        // if fn, arg names can not shadow fn name
        params->type.kind = kvoid;
        eachkid(params, param) {
            if (param->str == fnName) {
                addErr(
                    fmt("Function argument names can not shadow the function name (%{sym})", &fnName),
                    param->line, param->col
                );
                params->type.kind = kinvalid;
            }
        }
    };

    // if fn, vars can not shadow fn name
    if (fn->type.kind != kvoid) {
        eachkid(fn->kids[2], var) {
            if (var->str == fnName) {
                addErr(
                    fmt("Function variable names can not shadow the function name (%{sym})", &fnName),
                    var->line, var->col
                );
            }
        }
    }

    // declare variables
    aVars(fn->kids[2]);

    // analyze function body
    if (fn->kids[3] == NULL) {
        if (fn->type.kind != kvoid) {
            addErr(
                fmt("%{sym} is a %{type} meaning that its body can not be empty", &fnName, &fn->type),
                fn->line, fn->col
            );
        }
    } else {
        aCompound(fn->kids[3]);
    }

    if (fnSi->firstWrite == 0 && fnSi->type.retn != kvoid) {
        // if function, then there must be at least one assigment to it.
        addErr(
            fmt("Return value of %{sym} (%{type}) is never set", &fnName, &fnSi->type),
            fnSi->decl.line, fnSi->decl.col
        );
    } else if (fnSi->firstWrite != 0 && fnSi->type.retn == kvoid) {
        // if procedure, then there must be zero assigments to it.
        addErr(
            fmt("%{sym} (%{type}) is assigned, even though it doesn't return a value", &fnName, &fnSi->type),
            fnSi->decl.line, fnSi->decl.col
        );
    }

    /* argument clobbering analysis */ {
        for(size_t i=0; i<nArgs; i++) {
            if (params->kids[i]->type.isRef) {
                u32 name = params->kids[i]->str;
                SymInfo *argi = BST_get(local, name);

                // if it was written to
                if (argi->firstWrite) {
                    assert(fnSi->argprops);
                    fnSi->argprops[i].overwritten = 1;
                    // if it was read from, and the first write is after the first read
                    if (argi->firstRead && argi->firstWrite > argi->firstRead) {
                        fnSi->argprops[i].readfirst = 1; // then this argument is read before it is overwritten
                    }
                }
            }
        }
    }

    // exit local scope
#ifdef DBG
    printf("Current BST: %p\n", *current);
    BST_dbg(*current);
#endif
    current = &global;
    bst_to_be_freed[fbst++] = local;
    currfn = 0;
}

void declConst(AST *decl) {
    u32 ident = decl->kids[0]->str;
    AST *value = decl->kids[1];
    SymInfo *si = newSymInfo(value->type, value->line, value->col);
    if (value->type.kind == kint) {
        si->ival = value->ival;
    } else {
        si->dval = value->dval;
    }
    declare(ident, si);
}

// const decl list
void aConstants(AST *a) {
    eachkid(a, decl) {
        declConst(decl);
    }
    a->type.kind = kvoid;
}

// subprog decl list
void aSubs(AST *a) {
    eachkid(a, decl) {
        declFn(decl);
    }
    a->type.kind = kvoid;
}

void analyze(AST *a) {
    if (!a) crash("The parse tree was empty");

#ifdef DBG
    printAST(a,0);
#endif

    {u32 progid = a->kids[0]->str;}
    a->kids[0]->type.kind = kvoid;

    // ident, constdecllist, vardecllist, subprogdecllist, compound
    aConstants(a->kids[1]);
    aVars(a->kids[2]);
    aSubs(a->kids[3]);
    aCompound(a->kids[4]);
    a->type.kind = kvoid;

#ifdef DBG
    printf("Current BST: %p\n", *current);
    BST_dbg(*current);
    printAST(a,0);
#endif
    bst_to_be_freed[fbst++] = global;
}


void freeAllBSTs(void) {
    for(size_t i=0; i<fbst; i++) {
        freeBST(bst_to_be_freed[i]);
    }
}