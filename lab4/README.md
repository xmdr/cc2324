# The passes

1. Lexical analysis (lexer.fl). Checks number literals. Also builds up the string table
2. Grammatical analysis (parser.y) turns the input into a syntax tree using the BS grammar (BS = bison)
3. Semantic analysis (analyze.c) checks the syntax tree by building and checking symbol tables
4. Code generation (gen.c)

# Code structure

Somehow `main` ended up in `parser.y`. Just go with the flow bro.

`prelude.h` is the "i dont give a fuck just import the god damn shit you're the computer here you figure this out for me useless bitch" file.

`strings.c` is the string table (ternary search tree)

`diag.c` is for printing out the analysis results

`crash.c` is for crashing

`fmt.c` and `formatters.c` form a makeshift string formatting library and its usage

`ast.c` contains many AST wrangling utility functions

`symtab.c` is for symbol tables and binary search trees

`type.c` is kind of a misnomer. The interesting part is the `.h` file. Most typechecks are done inline in `analyze.c`.

# How to build

First, transpile the lexer and parser to C using flex and bison respectively. Then, each `.c` file compiles on its own.

The makefile automates this process.

# Minipas vs Pascal

- No nested subprograms
- Only 2 scope levels: global and subprogram
- No strings
- No pointers, only "references" and "slices" (regrettable as we shall see later)
- No goto
- You can not writeln nor readln an array

# Grammar

It would be a waste of pixels to reproduce it here. I used Meijster's reference parser grammar, unchanged. See `parser.y`.

# Type system

## Basic types

- real (implemented as C `double`)
- integer (implemented as C `int`)

`integer`s silently convert to `real` when an expression requires a `real` operand.

## Sub programs

- functions
- procedures (which are functions that do not evaluate to a value)

MiniPas does not have function pointers or function variables.

MiniPas does not support higher order functions.

Functions can only return a basic type.

## Mutability

- `VAR` (symbols that represent mutable values when declared globally, see Aliasing)
- `CONST` (untyped symbols that represent immutable numeric values)

All sub program arguments are always mutable.

All numeric literals are immutable values.

## Aliasing

- Passing by reference
- - The sub program takes an argument as `VAR`, which means aliasing is defined at the sub program site, instead of the call site (dumb)
- - A `VAR` function argument only accepts mutable values.
- Slices
- - You can take a slice of an array `arr[start_inclusive..end_inclusive]`. This smaller or equal array aliases the memory region of the original array. The slice must be indexed from `start_inclusive` to `end_inclusive`. 
- - Functions can take an "array". Which is always a reference slice of the original array.

## Arrays

Arrays are a mess. I'm sorry. It just is what it is.

This compiler only allows making slices with integer literals or constants.

Arrays have indexable ranges. Instead of indexing from 0, index from anywhere! fucking :galaxy_brain: move there Wirth!!! Pascal best language!!!

Passing an array as a function argument makes a copy (nice).

BUT! How would a divide and conquer algo work? It fucking wouldn't that's right! So to entangle this mess even further, when you pass an array into a function the array range gets fucking 'shifted' so that the lwb matches up to the lwb that the function argument defines. What the FUCK. And only when you declare the argument as VAR does the actual array unchanged get passed.

In short: passing `array [a..b]` as an argument of type `array [x..y]` will turn `[a,b]` into a `[a-a+x, b-a+x]` aka `[x, x+b-a]`. The compiler then checks if `(x+b-a) <= y`.

BUT: when taking a slice `arr[a..b]` of `array [x..y]` this translation does NOT happen.

# Lexical Scoping

Note: "symbol" means "name that refers to a value". When I state a rule about "symbols", I regard only the name.

- Only 2 scope levels: global and subprogram
- Global symbols (which excludes the program name) must all be unique.
- Subprogram symbols may be named equally to (shadow) global symbols. They may shadow the global program name, but not the subprogram name.
- Otherwise, symbols defined in sub programs must be unique within that subprogram

# Implemented checks

All the type rules are checked.

Integer and Real literals are checked for fitting in i64 and f64.

Arrays and slices are checked for range.

Array indexing is not checked.

Function calls are checked.

Passing constants as "var parameter", is checked (fancy! the checker checks if there occurs a write - if not, no warning, despite var)

Assignments are checked.

Guards and arithmetic expressions are checked.

Integers promote to Reals, and Reals demote to Integers with a warning.

# Codegen

This codegen is very ugly.

L/R values
- L-values are always emitted as registers of pointer type.
- A special "optimization" has been made for assigning to a variable. In that case, the variable name is directly used.
- any VAR function arguments (including readln) are L-values. Any assignment lhs is an L-value.
- Anything else is an R-value.

Array copies
- in this codegen, callees are responsible for making their local copy of the array in case the array arg is not VAR.
- mallocs a tmp, memcpy to tmp, then re-assigns the function argument(!) with that copy.
- Before return, the callee calls `free`.

Name mangling:
- all function/procedure names "s" are emitted as "%fn_s"
- all constant, variables and arguments of name "s" are emitted as "$s"
- all intermediate calculations are reified into temporary registers named with the pattern `$%d` to mimic LLVM style register naming.
- all control flow structures are turned into GOTO spaghetti, whose labels are named `L%d`.

# BUGS

The compiler does not always report correct line numbers and even more rarely correct column numbers. This is because the line and columns are recorded _after_ the grammatical term has been parsed fully. So the reported line and column numbers are those at the end of the problem area, not the start. This is a limitation of flex and bison.
