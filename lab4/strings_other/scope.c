#include "../prelude.h"

#define MAX_SCOPES 4096

static struct {
    BST *arr[MAX_SCOPES];
    size_t n;
} scopes = {{0},0};


u32 newScope(void) {
    if (scopes.n >= MAX_SCOPES) {
        crash("This compiler does not support more than " STR(MAX_SCOPES) " total scopes");
    }
    return scopes.n++;
}
