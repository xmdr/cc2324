#pragma once

#include "../prelude.h"

typedef u32 Scope;

SymInfo *getSym(Scope scope, u32 id);

Scope newScope(void);
