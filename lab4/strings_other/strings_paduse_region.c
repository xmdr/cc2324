#include "prelude.h"

// Packed ternary search tree, region allocated

// Since the struct was naturally padded anyway to align the pointers, 3 bytes were left over.
// This DS compares 32 bits instead of 8 bits at a time, storing up to 4 characters.
// Severe performance defects of DFS are noted when the tree has an "optimal" branching factor of 3.
// When lt is always null (which happens when building a string table from sorted strings),
// performance can increase 5 fold. starting at 2x at 50k random english words, ramping to 5x for 370k.
// Better memory locality plays a part (higher branching = nodes on the same level spread out farther in memory).
// Better branch prediction plays another part (lt always null is a check that gets predicted pretty well).
// For those situations, the naive single char DS micraculously performs better on little endian systems.
// Due to sorted strings being sorted differently when you 0 pad them _at the end_, and chunk them into 4-byte slices.

// Another interesting observation is that on shuffled input data, this padding-using version is 2x faster
// Another interesting observation is that using a region allocator (a giant array for all nodes, and node pointers turn into u32 indices)
// gets the struct down to 20 bytes. This weighs positively against the increased logic burden of having to deal with extra indirection.
// On shuffled data, it is up to 2x faster to use a region.

// Overal, the current version you are reading is 4x faster than a naive ternary search tree implementation.

// I speculate that this structure would additionaly benefit from
// - balancing mechanism
// - turning lt and eq into a single '<=' case
// - encoding the string in reverse (thereby having the padding work out beneficially on little endian systems)

// Merry christmas!
// - Michael Roeleveld, 25 december 2023

// Maximum length of a string
#define MAXLEN 64

// The search tree
typedef enum {LT=0, EQ=1, GT=2} Kid;
typedef struct TST TST;
struct TST {
    union {
        struct __attribute((packed)) {
        u32 lt, eq, gt;
        };
        u32 kids[3];
    };
    u32 id;
    union {
        u32 x;
        char xs[4];
    };
};

// Private global variables for the singleton string "table"
static u32 autoIncrement = 1;

// The memory region for all tree nodes
static struct {
    TST *base;
    size_t count, cap;
} region = {NULL, 0, 0};

static inline
void init(void){
    if (!region.cap){
        region.count = 1;
        region.cap   = 128;
        region.base  = reallocarray(region.base, region.cap, sizeof *region.base);
        region.base[0] = (TST){0};
    }
}

// Private function to create a new TST node
static inline
u32 newNode(u32 x) {
    // Make sure the region is large enough to accomodate allocation
    if (region.count+1 == region.cap){
        // grow the region by 100%
        region.cap *= 2;
        region.base = reallocarray(region.base, region.cap, sizeof *region.base);
    }

    // 0 is the null "pointer", and refs are expected to be stored in u32.
    // so we must return in the range [1, UINT32_MAX]
    if (++region.count >= UINT32_MAX){
        puts("String table has grown too large: more than 4.2 billion nodes. Consider writing a shorter program, or compiling in parts, or reusing variable names.");
        exit(1);
    }
    region.base[region.count] = (TST){.x=x};
    return region.count;
}

// Turn an index into a pointer
static inline
TST *ptrof(u32 idx){
    return region.base+idx;
}

static inline
u32 globalRoot(void){
    return ptrof(0)->kids[0];
}

// Private function to insert or retrieve a str-id pair in the TST
// The recursive version was only 0.9x as fast
static inline
int str2id(size_t n, const u32 str[n]){
    u32 nodeptr = 0;
    Kid kid = 0;

    u32 node;

    while(1){
        if (!(ptrof(nodeptr)->kids[kid])) {
            // if you do this in one line, it miscompiles
            // newNode can trigger a realloc of region.base
            // thereby invalidating ptrof(nodeptr)
            // which is apparently computed first
            u32 new = newNode(*str);
            ptrof(nodeptr)->kids[kid] = new;
        }
        node = ptrof(nodeptr)->kids[kid];

        // u32 cx = ptrof(node)->x;
        // u32 cs = *str;
        // strncmp is faster???
        int cmp = strncmp((char*)str, ptrof(node)->xs, 4);
        
        //if (cx < cs) {
        if (cmp < 0) {
            nodeptr = node;
            kid = LT;
        //} else if (cx > cs) {
        } else if (cmp > 0) {
            nodeptr = node;
            kid = GT;
        } else if (n > 1) {
            // Continue to the next character
            nodeptr = node;
            kid = EQ;
            n--;
            str++;
        } else {
            // String already exists, return existing id
            if (!ptrof(node)->id) {
                // If id is not assigned, assign an auto-incremented id
                ptrof(node)->id = autoIncrement++;
            }
            return ptrof(node)->id;
        }
    }
}

static inline
int id2str(u32 t, u32 *buf, u32 id){
    if (t) {
        if (ptrof(t)->id == id || id2str(ptrof(t)->eq, buf+1, id)){
            *buf = ptrof(t)->x;
            return 1;
        }

        if (id2str(ptrof(t)->lt, buf, id) || id2str(ptrof(t)->gt, buf, id)){
            return 1;
        }
    }

    return 0;
}

// Private freeing helper
static inline
void fhelp(void){
    free(region.base);
}

// Private debug stats
static struct {
    u32 maxd;
    u32 neq;
    u32 ngt;
    u32 nlt;
    u32 nnodes;
} stats;

// Printer helper
static inline
void phelp(u32 tp, u32 d){
    if (!tp) return;
    stats.maxd = d>stats.maxd?d:stats.maxd;
    stats.nnodes++;

    const TST t = *ptrof(tp);

    if (t.id){
        printf("p%u [label=\"'%.4s' (%u)\", color=blue]\n", tp, t.xs, t.id);
    } else {
        printf("p%u [label=\"'%.4s'\"]\n", tp, t.xs);
    }

    if (t.lt){
        i64 dist = sizeof(TST)*((i64)(t.lt)-(i64)tp);
        printf("p%u -> p%u [label=\"< (%ld bytes away)\"]\n", tp, t.lt, dist);
        phelp(t.lt, d+1);
        stats.nlt++;
    }
    if (t.eq){
        i64 dist = sizeof(TST)*((i64)(t.eq)-(i64)tp);
        printf("p%u -> p%u [label=\"= (%ld bytes away)\", color=blue]\n", tp, t.eq, dist);
        phelp(t.eq, d+1);
        stats.neq++;
    }
    if (t.gt){
        i64 dist = sizeof(TST)*((i64)(t.gt)-(i64)tp);
        printf("p%u -> p%u [label=\"> (%ld bytes away)\"]\n", tp, t.gt, dist);
        phelp(t.gt, d+1);
        stats.ngt++;
    }
}

//////////////
// Public API

// Outputs Graphviz DOT graph. Pipe the output into `xdot` to view.
void debugStrings(void){
    memset(&stats, 0, sizeof stats);
    puts("digraph g {");
    phelp(globalRoot(),1);
    printf("label=\"%u nodes of %lu bytes each = %lu bytes.\nRegion size: %lu slots = %lu bytes\nMaximum depth: %u. Edge counts: (%u <) (%u =) (%u >)\"\n",
        stats.nnodes, sizeof(TST), stats.nnodes*sizeof(TST), region.cap, region.cap*sizeof(TST), stats.maxd, stats.nlt, stats.neq, stats.ngt);
    puts("}");
}

// Free the string"table"
void freeStrings(void){
    fhelp();
}

// Get the string of a numerical ID
char *getString(u32 id){
    static u32 buf[MAXLEN/4+1] = {0};

    if (id2str(globalRoot(), buf, id)){
        return (char*)buf;
    } else {
        return NULL;
    }
}

// Get the numerical ID of a (new or existing) string
u32 getId(size_t len, const char str[len+1]){
    static u32 buf[MAXLEN/4+1] = {0};
    assert(len < MAXLEN);

    // align the input to 32 bits
    memset(buf, 0, sizeof buf);
    strcpy((char*)buf, str);
    
    size_t roundupLen = (len+3)/4;
    return str2id(roundupLen, buf);
}

void initStrings(void){
    init();
}

#ifdef TEST
int main(void){
    assert(sizeof(TST) == 20);
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(0, &cpuset);
    if (sched_setaffinity(getpid(), sizeof(cpu_set_t), &cpuset) == -1) {
        perror("Error setting CPU affinity");
        return 1;
    }

    init();

    int len = 0;
    static u32 buf[MAXLEN/4+1] = {0};
    assert(EOF != 1);
    while(scanf("%31s%n ", (char*)buf, &len) == 1){
        u32 id = str2id((len+3)/4, buf);
        assert(len < MAXLEN);
        printf("str '%s': %d\n", (char*)buf, id);
    }

    //debugStrings();

    for (u32 i=1; i<autoIncrement; ++i){
        id2str(globalRoot(), buf, i);
        printf("id %d: %s\n", i, (char*)buf);
    }

    fhelp();
}
#endif