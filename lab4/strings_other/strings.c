#include "prelude.h"

// Ternary search tree
typedef struct TST TST;
struct TST {
    TST *lt, *eq, *gt;
    u32 id;
    char x;
};

// Private global variables
static TST* globalRoot = NULL;
static int autoIncrement = 1;

static size_t nodect = 0;

// Function to create a new TST node
static inline
TST* newNode(char x) {
    nodect++;
    TST* node = malloc(sizeof(TST));
    node->x = x;
    node->id = 0;
    node->lt = node->eq = node->gt = NULL;
    return node;
}

// Function to insert or retrieve a str-id pair in the TST
static inline
int str2id(const char *str){
    TST **root = &globalRoot;
    int i = 0;
    TST *node;

    while(1){
        if (!(*root)) {
            *root = newNode(str[i]);
        }

        node = *root;
        char x = node->x;
        
        if (str[i] < x) {
            root = &node->lt;
        } else if (str[i] > x) {
            root = &node->gt;
        } else if (str[++i] != '\0') {
            // Continue to the next character
            root = &node->eq;
        } else {
            i--; // Undo that ++ above
            // String already exists, return existing id
            if (node->id == 0) {
                // If id is not assigned, assign an auto-incremented id
                node->id = autoIncrement++;
            }
            break;
        }
    }
    return node->id;
}

static inline
int str2id_rec(TST** root, const char* str, int i) {
    if (!(*root)) {
        *root = newNode(str[i]);
    }

    if (str[i] < (*root)->x) {
        return str2id_rec(&(*root)->lt, str, i);
    } else if (str[i] > (*root)->x) {
        return str2id_rec(&(*root)->gt, str, i);
    } else if (str[i + 1] != '\0') {
        // Continue to the next character
        return str2id_rec(&(*root)->eq, str, i + 1);
    } else {
        // String already exists, return existing id
        if ((*root)->id == 0) {
            // If id is not assigned, assign an auto-incremented id
            (*root)->id = autoIncrement++;
        }
        return (*root)->id;
    }
}

static inline
int id2str(TST *t, char *buf, int id){
    if (t) {
        if (t->id == id){
            buf[1] = '\0';
            buf[0] = t->x;
            return 1;
        }

        if (id2str(t->eq, buf+1, id)){
            buf[0] = t->x;
            return 1;
        }

        if (id2str(t->lt, buf, id) || id2str(t->gt, buf, id)){
            return 1;
        }

        // the desire to simplify the overlapping statements is palpable
    }

    return 0;
}


static inline
void fhelp(TST *t){
    if (!t) return;
    fhelp(t->eq);
    fhelp(t->lt);
    fhelp(t->gt);
    free(t);
}


int maxd = 0;
int neq = 0;
int ngt = 0;
int nlt = 0;

static inline
void phelp(TST *t, int d){
    maxd = d>maxd?d:maxd;
    if (t->id){
        printf("p%p [label=\"'%c' (%u)\", color=blue]\n", t, t->x, t->id);
    } else {
        printf("p%p [label=\"'%c'\"]\n", t, t->x);
    }

    if (t->eq){
        intptr_t dist = (intptr_t)(char*)t->eq-(intptr_t)(char*)t;
        printf("p%p -> p%p [label=\"= (%ld bytes away)\", color=blue]\n", t, t->eq, dist);
        phelp(t->eq, d+1);
        neq++;
    }
    if (t->lt){
        intptr_t dist = (intptr_t)(char*)t->lt-(intptr_t)(char*)t;
        printf("p%p -> p%p [label=\"< (%ld bytes away)\"]\n", t, t->lt, dist);
        phelp(t->lt, d+1);
        nlt++;
    }
    if (t->gt){
        intptr_t dist = (intptr_t)(char*)t->gt-(intptr_t)(char*)t;
        printf("p%p -> p%p [label=\"> (%ld bytes away)\"]\n", t, t->gt, dist);
        phelp(t->gt, d+1);
        ngt++;
    }
}

// Public API
void debugStrings(){
    puts("digraph g {");
    phelp(globalRoot,1);
    printf("label=\"%lu nodes of %lu bytes each = %lu bytes.\nMaximum depth: %d. Edge counts: (%d <) (%d =) (%d >)\"\n",nodect, sizeof(TST), nodect*sizeof(TST), maxd, nlt, neq, ngt);
    puts("}");
}

void freeStrings(){
    fhelp(globalRoot);
}

char *const getString(int id){
    static char buf[32] = {0};
    if (id2str(globalRoot, buf, id)){
        return buf;
    } else {
        return NULL;
    }
}

int getId(int len, const char str[len+1]){
    assert(len < 32);
    return str2id(str);
}

// Main function for testing
// reads in a bunch of words and then does a reverse lookup for all of them
#include <sched.h>
#include <unistd.h>
int main(void) {
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(0, &cpuset);
    if (sched_setaffinity(getpid(), sizeof(cpu_set_t), &cpuset) == -1) {
        perror("Error setting CPU affinity");
        return EXIT_FAILURE;
    }

    char buf[32];
    assert(EOF < 0);
    while(scanf("%31s", buf) == 1){
        int id = str2id(buf);
        printf("str '%s': %d\n", buf, id);
    }

    for (int i=1; i<autoIncrement; ++i){
        id2str(globalRoot, buf, i);
        printf("id %d: %s\n", i, buf);
    }

    fhelp(globalRoot);

    return 0;
}
