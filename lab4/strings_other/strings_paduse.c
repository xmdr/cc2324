#include "prelude.h"

// Packed ternary search tree

// Since the struct was naturally padded anyway to align the pointers, 3 bytes were left over
// This DS compares 32 bits instead of 8 bits at a time, storing up to 4 characters
// Severege performance defects of DFS are noted when the tree has "optimal" branching factor of 3
// When lt is always null (which happens when building a string table from sorted strings),
// performance can increase 5 fold. starting at 2x at 50k random english words, ramping to 5x for 370k.
// Better memory locality plays a part (higher branching = nodes on the same level spread out farther in memory)
// Better branch prediction plays another part (lt always null is a check that gets predicted pretty well)
// For those situations, the single char DS micraculously performs better on little endian systems.
// Due to sorted strings being sorted differently when you 0 pad them _at the end_, and chunk them into 4-byte slices.

// Another interesting observation is that on shuffled input data, this version is 2x faster
// Another interesting observation is that when using a region allocator (a giant array for all nodes, and node pointers turn into u32 indices)
// gets the struct down to 20 bytes. This weighs positively against the increased logic burden of having to deal with extra indirection.
// On shuffled data, it is up to 1.1x faster to use a region.

// I speculate that this structure would benefit from
// - balancing mechanism
// - turning lt and eq into a single '<=' case
// - encoding the string in reverse (thereby having the padding work out beneficially on little endian systems)

// Merry christmas!
// - Michael Roeleveld, 25 december 2023

// Maximum length of a string
#define MAXLEN 64

// The search tree
typedef struct TST TST;
struct TST {
    TST *lt, *eq, *gt;
    u32 id;
    union {
        u32 x;
        char xs[4];
    };
};

// Private global variables for the singleton string "table"
static TST* globalRoot = NULL;
static u32 autoIncrement = 1;

// Private function to create a new TST node
static inline
TST* newNode(u32 x) {
    TST* node = malloc(sizeof(TST));
    *node = (TST){.x=x};
    return node;
}

// Private function to insert or retrieve a str-id pair in the TST
static inline
u32 str2id(size_t n, const u32 str[n]){
    TST **root = &globalRoot;

    while(1){
        if (!(*root)) {
            *root = newNode(*str);
        }

        TST *node = *root;
        u32 x = node->x;

        if (*str < x) {
            root = &node->lt;
        } else if (*str > x) {
            root = &node->gt;
        } else if (n > 1) {
            // Still input left, make another eq node
            root = &node->eq;
            n--;
            str++;
        } else {
            // Fully matched, return existing id
            if (node->id == 0) {
                // If id is not assigned, assign an auto-incremented id
                node->id = autoIncrement++;
            }
            return node->id;
        }
    }
}

static inline
u32 str2id2(TST **root, size_t n, const u32 str[n]){
    if (!(*root)) {
        *root = newNode(*str);
    }

    TST *node = *root;
    u32 x = node->x;

    if (*str < x) {
        return str2id2(&node->lt, n, str);
    }
    if (*str > x) {
        return str2id2(&node->gt, n, str);
    }
    if (n > 1) {
        // Still input left, make another eq node
        return str2id2(&node->eq, n-1, str+1);
    }
    // Fully matched, return existing id
    if (node->id == 0) {
        // If id is not assigned, assign an auto-incremented id
        node->id = autoIncrement++;
    }
    return node->id;
}

static inline
int id2str(TST *t, u32 *buf, u32 id){
    if (t) {
        if (t->id == id || id2str(t->eq, buf+1, id)){
            *buf = t->x;
            return 1;
        }

        if (id2str(t->lt, buf, id) || id2str(t->gt, buf, id)){
            return 1;
        }
    }

    return 0;
}

// Private freeing helper
static inline
void fhelp(TST *t){
    if (!t) return;
    fhelp(t->eq);
    fhelp(t->lt);
    fhelp(t->gt);
    free(t);
}

// Private debug stats
static struct {
    u32 maxd;
    u32 neq;
    u32 ngt;
    u32 nlt;
    u32 nnodes;
} stats;

// Printer helper
static inline
void phelp(TST *t, u32 d){
    if (!t) return;
    stats.maxd = d>stats.maxd?d:stats.maxd;
    stats.nnodes++;
    if (t->id){
        printf("p%p [label=\"'%.4s' (%u)\", color=blue]\n", (void*)t, t->xs, t->id);
    } else {
        printf("p%p [label=\"'%.4s'\"]\n", (void*)t, t->xs);
    }

    if (t->eq){
        intptr_t dist = (intptr_t)(char*)t->eq-(intptr_t)(char*)t;
        printf("p%p -> p%p [label=\"= (%ld bytes away)\", color=blue]\n", (void*)t, (void*)t->eq, dist);
        phelp(t->eq, d+1);
        stats.neq++;
    }
    if (t->lt){
        intptr_t dist = (intptr_t)(char*)t->lt-(intptr_t)(char*)t;
        printf("p%p -> p%p [label=\"< (%ld bytes away)\"]\n", (void*)t, (void*)t->lt, dist);
        phelp(t->lt, d+1);
        stats.nlt++;
    }
    if (t->gt){
        intptr_t dist = (intptr_t)(char*)t->gt-(intptr_t)(char*)t;
        printf("p%p -> p%p [label=\"> (%ld bytes away)\"]\n", (void*)t, (void*)t->gt, dist);
        phelp(t->gt, d+1);
        stats.ngt++;
    }
}

// Main function for testing
// reads in a bunch of words and then does a reverse lookup for all of them
// quadratic complexity
static inline
void test(void) {
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(0, &cpuset);
    if (sched_setaffinity(getpid(), sizeof(cpu_set_t), &cpuset) == -1) {
        perror("Error setting CPU affinity");
        return;
    }

    int len;
    static u32 buf[MAXLEN/4+1] = {0};
    assert(EOF < 0);
    while(scanf("%31s%n", (char*)buf, &len) == 1){
        u32 id = str2id2(&globalRoot, (len+3)/4,buf);
        assert(len < MAXLEN);
        printf("str '%s': %d\n", (char*)buf, id);
    }

    for (u32 i=1; i<autoIncrement; ++i){
        id2str(globalRoot, buf, i);
        printf("id %d: %s\n", i, (char*)buf);
    }

    fhelp(globalRoot);
}

//////////////
// Public API

// Outputs Graphviz DOT graph. Pipe the output into `xdot` to view.
void debugStrings(void){
    memset(&stats, 0, sizeof stats);
    puts("digraph g {");
    phelp(globalRoot,1);
    printf("label=\"%u nodes of %lu bytes each = %lu bytes.\nMaximum depth: %u. Edge counts: (%u <) (%u =) (%u >)\"\n",
        stats.nnodes, sizeof(TST), stats.nnodes*sizeof(TST), stats.maxd, stats.nlt, stats.neq, stats.ngt);
    puts("}");
}

// Free the string"table"
void freeStrings(void){
    fhelp(globalRoot);
}

// Get the string of a numerical ID
char *getString(u32 id){
    static u32 buf[MAXLEN/4+1] = {0};

    if (id2str(globalRoot, buf, id)){
        return (char*)buf;
    } else {
        return NULL;
    }
}

// Get the numerical ID of a (new or existing) string
u32 getId(size_t len, const char str[len+1]){
    static u32 buf[MAXLEN/4+1] = {0};
    assert(len < MAXLEN);

    // align the input to 32 bits
    memset(buf, 0, sizeof buf);
    strcpy((char*)buf, str);
    
    size_t roundupLen = (len+3)/4;
    return str2id(roundupLen, buf);
}

int main(){
    test();
}