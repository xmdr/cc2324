#include "prelude.h"

// Ternary search tree, indirectly addressed through the region, with up to 4 inline characters
typedef struct TST TST;
struct TST {
    u32 lt, gt, eq, id;
    char x[4]; // only x[0] is used... for now
};

////////////////////////////
// Private global variables

// ID Auto increment and the current root node of the TST
static u32 globalRoot = 0;
static u32 autoIncrement = 1;

// The memory region for all tree nodes
static struct {
    TST *base;
    size_t count, cap;
} region = {NULL, 0, 0};

////////////////////////////
// Allocator
static inline
u32 alloc(TST t){
    // Make sure the region is large enough to accomodate allocation
    if (region.count+1 == region.cap){
        // grow the region by 100%
        region.cap *= 2;
        printf("reallocating: %lu\n", region.cap);
        printf("reallocating: %lu\n", region.cap*sizeof *region.base);
        region.base = reallocarray(region.base, region.cap, sizeof *region.base);
    }

    // 0 is the null "pointer", and refs are expected to be stored in u32.
    // so we must return in the range [1, UINT32_MAX]
    if (++region.count >= UINT32_MAX){
        puts("String table has grown too large: more than 4.2 billion nodes. Consider writing a shorter program, or compiling in parts, or reusing variable names.");
        exit(1);
    }
    region.base[region.count] = t;
    return region.count;
}

static inline
TST *ptrof(u32 idx){
    return region.base+idx;
}

// Function to create a new TST node
static inline
u32 newNode(char x) {
    return alloc((TST){0,0,0,0,.x={x,0}});
}

static inline
void init(){
    region.count = 0;
    region.cap = 128;
    region.base = malloc(128*sizeof *region.base);
    globalRoot = alloc((TST){0});
}

// Function to insert or retrieve a str-id pair in the TST
static inline
int str2id(const char *str){
    u32 node;
    u32 next = globalRoot;
    u32 i = 0;

    while((node = next)){
        char x = ptrof(node)->x[0];

        if (str[i] < x) {
            if (ptrof(node)->lt == 0){
                next = newNode(str[i]);
                ptrof(node)->lt = next;
            } else {
                next = ptrof(node)->lt;
            }
        } else if (str[i] > x) {
            if (ptrof(node)->gt == 0){
                next = newNode(str[i]);
                ptrof(node)->gt = next;
            } else {
                next = ptrof(node)->gt;
            }
        } else if (str[++i] != '\0') {
            // Continue to the next character
            if (ptrof(node)->eq == 0){
                next = newNode(str[i]);
                ptrof(node)->eq = next;
            } else {
                next = ptrof(node)->eq;
            }
        } else {
            i--; // Undo that ++ above
            // String already exists, return existing id
            if (ptrof(node)->id == 0) {
                // If id is not assigned, assign an auto-incremented id
                ptrof(node)->id = autoIncrement++;
            }
            break;
        }
    }
    return ptrof(node)->id;
}

static inline
int id2str(u32 t, char *buf, u32 id){
    if (t) {
        if (ptrof(t)->id == id){
            buf[1] = '\0';
            buf[0] = ptrof(t)->x[0];
            return 1;
        }

        if (id2str(ptrof(t)->eq, buf+1, id)){
            buf[0] = ptrof(t)->x[0];
            return 1;
        }

        if (id2str(ptrof(t)->lt, buf, id) || id2str(ptrof(t)->gt, buf, id)){
            return 1;
        }

        // the desire to simplify the overlapping statements is palpable
    }

    return 0;
}


// Public API
void initStrings(){
    init();
}

void freeStrings(){
    free(region.base);
}

char *const getString(u32 id){
    static char buf[32] = {0};
    if (id2str(globalRoot, buf, id)){
        return buf;
    } else {
        return NULL;
    }
}

u32 getId(size_t len, const char str[len+1]){
    assert(len < 32);
    return str2id(str);
}

// Main function for testing
// reads in a bunch of words and then does a reverse lookup for all of them
#include <sched.h>
#include <unistd.h>
int main(void) {
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(0, &cpuset);
    if (sched_setaffinity(getpid(), sizeof(cpu_set_t), &cpuset) == -1) {
        perror("Error setting CPU affinity");
        return EXIT_FAILURE;
    }

    initStrings();

    char buf[32];
    assert(EOF < 0);
    while(scanf("%31s", buf) == 1){
        printf("str '%s': %d\n", buf, str2id(buf));
    }

    for (u32 i=1; i<autoIncrement; ++i){
        id2str(globalRoot, buf, i);
        printf("id %d: %s\n", i, buf);
    }

    freeStrings();

    return 0;
}
