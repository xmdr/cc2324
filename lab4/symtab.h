#pragma once

#include "prelude.h"

typedef struct BST BST;
typedef struct SymInfo SymInfo;

// AVL tree for the symbol table
struct BST {
	BST *lt, *gt;
	SymInfo *info;
	u32 id;
	u32 height;
};

typedef struct {
	int line;
	int col;
} Location;

// Symbol information
struct SymInfo {
	// syminfo of a function scope also contains the function arguments as variables in that scope.
	// their type* alias with the type* of that functions' type.
	Type type;
	Location decl;
	
	// In case type is a fn or proc
	// Pascal does not have function pointers, so these are truly mutually exclusive cases
	struct {
		u32 name;
		// behavior of arguments passed as reference
		u32 overwritten:1; // does the procedure overwrite the variable (i.e., can not be const)
		u32 readfirst:1;  // does the variable need to be initialized to be passed safely
	} *argprops; // argprops[this.type->nArgs]
	// In case of const
	union {
		i32 ival;
		f32 dval;
	};
	// In case type is anything else
	struct {
		u32 firstWrite; // line number where this variable is first set by := or write()
		u32 firstRead; // line number where this variable is first used
		u32 isArg:1; // is function argument
		u32 argPos:32; // the argument position
	};
};

SymInfo *newSymInfo(Type t, int line, int col);

// Add an identifier to a tree
// BST *t = NULL;
// t = addsym(t, id, &info);
BST *BST_set(BST *node, u32 id, SymInfo *info, jmp_buf onDuplicate);

// Get the symbol info associated with an identifier, or NULL
SymInfo *BST_get(BST *root, u32 id);

void BST_dbg(BST *root);

// Free a tree
void freeBST(BST *t);
