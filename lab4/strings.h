#include "prelude.h"

// debug the string table
void debugStrings(void);

// run before using
void initStrings(void);

// run before exit
void freeStrings(void);

// turn an id into a string
// WARNING reuses an internal buffer. Use strdup to "keep".
char *getString(u32 id);

// turn a string into an id
// len is the string length, so exclude the 0 byte
u32 getId(size_t len, const char str[len+1]);

/*
int main(){
	initStrings();
	char *str;
	while(scanf("%ms", &str) == 1){
		getId(strlen(str), str);
		free(str);
	}

	debugStrings();

	freeStrings();
}
*/