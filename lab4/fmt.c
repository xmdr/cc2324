#include "prelude.h"

// Do you really need more?
#define MAX_FORMATTERS 64

// singleton pattern
static struct {
    const char *triggers[MAX_FORMATTERS]; // lazy fuck pattern
    Formatter handlers[MAX_FORMATTERS];
    size_t n;
} fmts = {{0}, {0}, 0};

void addFormat(const char *trigger, Formatter formatter){
    // smelly shotgun surgery
    if (fmts.n == MAX_FORMATTERS){
        crash("Attempted to register a string formatter, but the array is full");
    }
    fmts.triggers[fmts.n  ] = trigger;
    fmts.handlers[fmts.n++] = formatter;
}

static inline
Formatter getFormatter(size_t len, const char trigger[len]){
    for (size_t i=0; i<fmts.n; i++){
        if (!strncmp(trigger, fmts.triggers[i], len))
            return fmts.handlers[i];
    }
    return NULL;
}

// for debugging
static inline
void printFormats(void){
    for (size_t i=0; i<fmts.n; i++){
        printf("[%lu]: %p handles {%s}\n", i, fmts.handlers[i], fmts.triggers[i]);
    }
}

// realloc dst, and free src
static inline
void concat(size_t *n, char *dst[*n], size_t m, char src[m]){
    *dst = realloc(*dst, *n+m+1);
    strcpy(*dst+*n, src);
    *n += m;
    free(src);
}

char *fmt(const char* format, ...) {
    va_list args;
    va_start(args, format);
    size_t len = 0;
    char *res = NULL;
    while (*format) {
        if (*format == '%') {
            switch (*++format) {
                #define c(f, fmt, typ) case f: {\
                    char *tmp;\
                    size_t tmplen = asprintf(&tmp, fmt, va_arg(args, typ));\
                    concat(&len, &res, tmplen, tmp);\
                    break;\
                }
                c('d', "%d", int);
                c('u', "%d", unsigned int);
                c('f', "%f", double);
                c('c', "%d", int);
                c('p', "%p", void*);
                c('s', "%s", char*);
                c('l', "%ls", long long);
                c('z', "%lu", size_t);
                #undef c
                case '{': {
                    const char *word = ++format;
                    format = strchr(format, '}');
                    size_t wordlen = format-word;
                    Formatter formatter = getFormatter(wordlen, word);
                    if (!format){
                        crash("Attempted to format a string, but the closing } was not found in the format string.");
                    }
                    if (!formatter){
                        crash("Attempted to format a string, but the requested format was not defined");
                    }
                    char *tmp; size_t tmplen;
                    formatter(va_arg(args, void*), &tmp, &tmplen);
                    concat(&len, &res, tmplen, tmp);
                    break;
                }
                default:{
                    char *tmp;
                    size_t n = asprintf(&tmp, "%%%c", *format);
                    concat(&len, &res, n, tmp);
                    break;
                }
            }
        } else {
            char *tmp;
            size_t n = asprintf(&tmp, "%c", *format);
            concat(&len, &res, n, tmp);
        }
        format++;
    }
    va_end(args);
    return res;
}

