#include "prelude.h"

#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_RED    "\x1b[31m"
#define ANSI_COLOR_RESET  "\x1b[0m"

#define MAX_DIAGS 32

typedef struct {
    char *msg;
    u32 line;
    u32 col;
} Diagnostic;

typedef enum {warnings=0, errors=1, n_diagkinds} Diagkind;

static struct {
    Diagnostic arr[MAX_DIAGS];
    size_t n;
    const char *const ansi_color;
    const char *const name;
} diags[n_diagkinds] = {
    {{0},0,ANSI_COLOR_YELLOW, "WARNINGS"},
    {{0},0,ANSI_COLOR_RED,    "ERRORS"}
};

static inline
void addDiag(Diagkind which, char *msg, int line, int col) {
    if (diags[which].n < MAX_DIAGS){
        diags[which].arr[diags[which].n] = (Diagnostic){msg, line, col};
    } else {
        free(msg);
    }
    diags[which].n++;
}

void addWarn(char *msg, int line, int col){ addDiag(warnings, msg, line, col); }
void addErr (char *msg, int line, int col){ addDiag(errors,   msg, line, col); }

static inline
void printDiag(Diagnostic d) {
    fprintf(stderr,  "line %d (col %d): %s\n", d.line, d.col, d.msg);
    free(d.msg);
    d.msg = NULL;
}

/**
 * \brief print all diagnostics on stderr, and a warning/error summary on stdout
 */
void diagnosis(void) {
    for (size_t k=0; k<n_diagkinds; k++) {
        fprintf(stderr, diags[k].ansi_color);

        for (size_t d=0; d<diags[k].n; d++)
            printDiag(diags[k].arr[d]);

        if (diags[k].n > MAX_DIAGS)
            fprintf(stderr, "NOTE: Only the first %u %s have been reported.\n", MAX_DIAGS, diags[k].name);

        fprintf(stderr, ANSI_COLOR_RESET);

        fprintf(stderr,"%s: %lu\n", diags[k].name, diags[k].n);
    }

    fprintf(stderr, diags[errors].n ? "REJECTED\n" : "ACCEPTED\n");
}
