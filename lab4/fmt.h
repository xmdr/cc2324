#pragma once
#include "prelude.h"

// A formatter takes a single pointer to its data. Internally, it is to overwrite a string.
// *n = asprintf(overwrite, "stuff", args)
typedef void(*Formatter)(void *data, char **overwrite, size_t *bytes_written);

// Register a format, "%{trigger}". The first registered "x" trigger gets to handle the "%{x}"
void addFormat(const char *trigger, Formatter formatter);

// Format a string. Returns allocated string. Also supports simple printf formats. Pass the pointers that the formatters expect.
char *fmt(const char *format, ...);
