#include "prelude.h"

static inline
u32 height(BST *t){
	return t?t->height:0;
}

static inline
u32 max(u32 a, u32 b){
	return a>b?a:b;
}

static inline
BST *newBST(u32 id, SymInfo *info){
	BST *t = malloc(sizeof*t);
	*t = (BST){.lt=NULL,.gt=NULL,.info=info,.id=id,.height=1};
	return t;
}

static inline
BST *rotR(BST *y){
	BST *x   = y->lt;
	BST *tmp = x->gt;
	x->gt = y;
	y->lt = tmp;
	y->height = max(height(y->lt), height(y->gt))+1;
	x->height = max(height(x->lt), height(x->gt))+1;
	return x;
}

static inline
BST *rotL(BST *x){
	BST *y   = x->gt;
	BST *tmp = y->lt;
	y->lt = x;
	x->gt = tmp;
	x->height = max(height(x->lt), height(x->gt))+1;
	y->height = max(height(y->lt), height(y->gt))+1;
	return y;
}

// Insert a id into the AVL tree
BST *BST_set(BST *node, u32 id, SymInfo *info, jmp_buf duplicate) {
	// Perform normal BST insert
	if (node == NULL)
		return newBST(id, info);

	if (id < node->id)
		node->lt = BST_set(node->lt, id, info, duplicate);
	else if (id > node->id)
		node->gt = BST_set(node->gt, id, info, duplicate);
	else { // Duplicate ids not allowed
		longjmp(duplicate,1);
	}
	// Update height of current node
	u32 hl = height(node->lt), hg = height(node->gt);
	node->height = 1 + max(hl, hg);

	// Get the balance factor to check if this node became unbalanced
	int balance = hl-hg;

	// lt lt Case
	if (balance > 1 && id < node->lt->id)
		return rotR(node);

	// gt gt Case
	if (balance < -1 && id > node->gt->id)
		return rotL(node);

	// lt gt Case
	if (balance > 1 && id > node->lt->id) {
		node->lt = rotL(node->lt);
		return rotR(node);
	}

	// gt lt Case
	if (balance < -1 && id < node->gt->id) {
		node->gt = rotR(node->gt);
		return rotL(node);
	}

	// Return the unchanged node pointer
	return node;
}

// Search for a id in the AVL tree
SymInfo *BST_get(BST *root, u32 id) {
	if (!root) return NULL;
	if (root->id == id)
		return root->info;

	if (id > root->id)
		return BST_get(root->gt, id);

	return BST_get(root->lt, id);
}

void freeBST(BST *t){
	if (!t) return;
	freeBST(t->gt);
	freeBST(t->lt);
	if (t->info->type.kind == kfn) {
		free(t->info->argprops);
		free(t->info->type.argTypes);
	}
	free(t->info);
	free(t);
}

// Print the inorder traversal of the AVL tree
static inline
void dbgBST(BST *root) {
	if (root != NULL) {
		dbgBST(root->lt);
		printf("%d: %lu\n", root->id, (uintptr_t)root->info);
		dbgBST(root->gt);
	}
}

// real	27m2.608s
// user	26m51.328s
// sys	0m0.675s

// with core pegged, shared ALU core disabled

// real	26m19.771s
// user	26m4.644s
// sys	0m5.201s

static inline
void testBST(void){
	const size_t n = 32;
	struct {
		u32 id;
		SymInfo *val;
	} pairs[n];
	puts("Pairs:");
	for (size_t i=0; i<n; ++i){
		pairs[i].id = i+1;
		pairs[i].val = (void*)(i+10001);
		printf("id:%d\tval:%lu\n", pairs[i].id, (uintptr_t)pairs[i].val);
	}

	BST *t = NULL;
	for (size_t i=0; i<n; ++i){
		jmp_buf dupex;
		if (setjmp(dupex)) {
			puts("duplicate found");
			exit(1);
		}
		t = BST_set(t, pairs[i].id, pairs[i].val, dupex);
		
		puts("\nTable:");
		dbgBST(t);
	
		puts("\nPositives:");
		for (size_t j=0; j<=i; ++j){
			u32 id = pairs[j].id;
			SymInfo *val = pairs[j].val;
			printf("id:%u, v:%lu\n", id,(uintptr_t)val);
			t = BST_set(t, id, val, dupex);
			assert(BST_get(t,id) == val);
		}
		puts("\nNegatives:");
		for (size_t j=i+1; j<n; ++j){
			u32 id = pairs[j].id;
			SymInfo *val = BST_get(t,id);
			printf("id:%u, v:%lu\n", id,(uintptr_t)val);
			assert(val == NULL);
		}
	}

	freeBST(t);
}


SymInfo *newSymInfo(Type t, int line, int col) {
	SymInfo *si = calloc(1,sizeof *si);
	si->decl.line = line;
	si->decl.col = col;
	si->type = t;
	return si;
}

void BST_dbg(BST *root) {
	if (!root) return;
	BST_dbg(root->lt);
	{
		SymInfo *s = root->info;
		char *p = fmt("%{sym} of %{type} declared on line %d", &root->id, &s->type, s->decl.line);
		puts(p);
		free(p);
	}
	BST_dbg(root->gt);
}
