PROGRAM x;
FUNCTION return42(n : integer) : integer;
BEGIN
	IF n > 42 THEN
	return42 := return42(n-1)
	ELSE BEGIN return42 := n
		; WHILE return42 < 42 DO return42 := return42 + 1
	END
END;

BEGIN
	writeln(return42(3))
END.