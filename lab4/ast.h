#pragma once

#include "prelude.h"

// fat as fuck struct
typedef struct AST AST;
struct AST {
    Type type;
    const char *kind; // sorry man, I just can not be assed having to define the same words over and over
    int line;
    int col;
    union {
        f64 dval;
        i64 ival;
        u32 str;
    };
    size_t nKids;
    AST *kids[];
};

AST *newAST(const char *kind, const size_t nKids, ...);

AST *newIntLit(const i64 val);

AST *newRealLit(const f64 val);

AST *newIdent(const u32 id);

AST *newTerm(const char *kind);

void printAST(const AST *a, const int d);

void freeAST(AST *a);

void printToken(int token, FILE *f);

// frees old. If old is a child, that will break stuff.
AST *addKid(AST *old, AST *kid);

// frees old, reallocs usurper. Kids of old get added to kids of usurper.
AST *usurpKids(AST *usurper, AST *old);

void cascadeType(AST *a);

AST *newTypeAST(const Type t);

AST *newBinop(const char *kind, AST *l, AST *r);
AST *newUnop(const char *kind, AST *l);
