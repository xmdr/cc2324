#pragma once

#define _GNU_SOURCE

#include <math.h>
#include <stdio.h>
#include <ctype.h>
#include <sched.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <setjmp.h>

#define STR(x) #x

typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;

typedef int64_t i64;
typedef int32_t i32;
typedef int16_t i16;
typedef int8_t i8;

typedef long double f80;
typedef double f64;
typedef float f32;

#include "crash.h"
#include "fmt.h"
#include "strings.h"
#include "type.h"
#include "symtab.h"
#include "ast.h"
#include "diag.h"
#include "analyze.h"
#include "formatters.h"
#include "gen.h"
