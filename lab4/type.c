#include "prelude.h"

Type newType_arr(Kind elem, i32 lwb, i32 upb) {
	if (lwb > upb) {
		extern int linenr, colnr; // from lexer
		addErr(fmt("attempt to create array spanning [%d..%d], but %d > %d which is not allowed",
			lwb,upb,lwb,upb), linenr, colnr);
	}                                      return (Type){.kind=karr,  .lwb=lwb, .upb=upb, .elem=elem}; }
Type newType_int (u32 isRef, u32 isConst){ return (Type){.kind=kint,  .isRef=isRef, .isConst=isConst}; }
Type newType_real(u32 isRef, u32 isConst){ return (Type){.kind=kreal, .isRef=isRef, .isConst=isConst}; }
Type newType_undecided             (void){ return (Type){.kind=kundecided                           }; }

void typeCheck(const Type g, const Type e, char **warn, char **err){
	if ((g.kind == e.kind)                         // obviously OK
	||  (g.kind == kinvalid || e.kind == kinvalid) // already failed, no need to spam errors
	||	(g.kind == kint && e.kind == kreal))       // converting an int to real is OK
		return;

	if (g.kind == kreal && e.kind == kint){
		*warn = fmt("conversion of real to int truncates the value.");
	} else if (g.kind == kvoid){
		*err = fmt("procedures do not return a value.");
	} else if (e.kind == kvoid){
		*warn = fmt("%{type} value was discarded.", g);
	} else if (g.kind == karr){
		// check arr[x..y], x >= arr.lwb && y <= arr.upb
		// note:  expect arr[x..y] get arr[a..b]
		//        translate a..b to x..x+b-a
		int erange = e.upb - e.lwb;
		int grange = g.upb - g.lwb;

		if (grange < 1) {
			*err = fmt("the given array %{type} has invalid index range", g);
		} else if (erange < 1) {
			*err = fmt("the expected array %{type} has invalid index range", g);
		} else if (grange < erange){
			*err = fmt("array types have disjoint ranges: expected %{type} but got %{type}", e, g);
		} else if (g.elem != e.elem) {
			*err = fmt("array types have different base types: expected %{type} but got %{type}", e, g);
		}
	} else {
		*err = fmt("%{type} was provided, while %{type} was expected.", g, e);
	}
}

static inline
void concat(size_t *n, char *dst[*n], size_t m, char src[m]) {
	*dst = realloc(*dst, *n+m+1);
	strcpy(*dst+*n, src);
	*n += m;
	free(src);
}

static inline
const char *kind2str(Kind k){
	switch(k) {
		case kint: return "INTEGER";
		case kreal: return "REAL";
		case kbool: return "BOOLEAN";
		case kvoid: return "<no value>";
		case kinvalid: return "<type error>";
		case kundecided: return "<\033[1mNOT YET CHECKED\033[0m>";
		default: {
			crash(fmt("Bad type kind %d", k));
			__builtin_unreachable();
		}
	}
}

void fmtType(Type *t, char **dst, size_t *n) {
	if ((uintptr_t)t < 999) {
		*n = asprintf(dst, "`NULL %p`", t);
		return;
	}
	if (t->kind == karr) {
		*n = asprintf(dst, "ARRAY [%d..%d] OF %s", t->lwb, t->upb, kind2str(t->elem));
		return;
	}
	if (t->kind == kfn) {
		char *argsStr = NULL;
		size_t argsStrLen = 0;
		for (u32 i=0, nArgs=t->nArgs; i<nArgs; i++) {
			char *arg;
			if (i+1 == nArgs) {
				arg = fmt("%{type}", &t->argTypes[i]);
			} else {
				arg = fmt("%{type}, ", &t->argTypes[i]);
			}
			size_t argLen = strlen(arg); // ugh I hate C
			concat(&argsStrLen, &argsStr, argLen, arg);
		}
		if (t->retn != kvoid) {
			*n = asprintf(dst, "FUNCTION (%.*s): %s", (int)argsStrLen, argsStr, kind2str(t->retn));
		} else {
			*n = asprintf(dst, "PROCEDURE (%.*s)", (int)argsStrLen, argsStr);
		}
		free(argsStr);
		return;
	}
	if (t->isRef && t->isConst) {
		*n = asprintf(dst, "`VAR CONST (what?) %s`", kind2str(t->kind));
	} else if (t->isRef) {
		*n = asprintf(dst, "`VAR %s`", kind2str(t->kind));
	} else if (t->isConst) {
		*n = asprintf(dst, "`CONST %s`", kind2str(t->kind));
	} else {
		*n = asprintf(dst, kind2str(t->kind));
	}
}