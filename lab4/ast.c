#include "prelude.h"

#include "parser.tab.h"

// from lexer
extern int linenr;
extern int colnr;

static inline
size_t ASTsize(const AST *a) {
    return a->nKids*(sizeof *a->kids) + (sizeof *a);
}

AST *newAST(const char *kind, const size_t nKids, ...){
    va_list kids;
    va_start(kids, nKids);
    AST *a = calloc(1, nKids*sizeof(a) + sizeof *a);
    *a = (AST){.kind=kind, .nKids=nKids};
    for (size_t i=0; i<nKids; i++) {
        a->kids[i] = va_arg(kids, AST*);
    }
    va_end(kids);
    a->type = newType_undecided();
    a->line = linenr;
    a->col = colnr;
    return a;
}

AST *newIntLit(const i64 val) {
    AST *a = newAST("intlit", 0);
    a->type = newType_int(0,1);
    a->ival=val;
    return a;
}

AST *newRealLit(const f64 val) {
    AST *a = newAST("reallit", 0);
    a->type = newType_real(0,1);
    a->dval=val;
    return a;
}

AST *newIdent(const u32 id) {
    AST *a = newAST("ident", 0);
    a->str=id;
    return a;
}

AST *newTypeAST(const Type t) {
    AST *r = newAST("Type", 0);
    r->type = t;
    return r;
}

AST *newTerm(const char *kind) {
    return newAST(kind, 0);
}

// slowly transform a linked list into a normal array
AST *addKid(AST *old, AST *kid) {
    if (!kid) return old;
    if (!old) return kid;
    AST *a = newAST(old->kind, old->nKids+1);
    memcpy(a, old, ASTsize(old));
    a->nKids = old->nKids+1;
    a->kids[old->nKids] = kid;
    free(old);
    return a;
}

// usurper inherits all kids from old (which is freed)
AST *usurpKids(AST *usurper, AST *old) {
    if (!usurper) return old;
    if (!old) return usurper;
    const size_t totalKids = usurper->nKids + old->nKids;

    usurper = realloc(usurper, totalKids*sizeof(AST*) + sizeof (AST));

    for (size_t i=0; i<old->nKids; i++) {
        usurper->kids[i+usurper->nKids] = old->kids[i];
    }
    usurper->nKids = totalKids;
    free(old);
    return usurper;
}

// make all kids of the same type
void cascadeType(AST *a) {
    if (!a) return;

    for (size_t i=0; i<a->nKids; i++) {
        a->kids[i]->type = a->type;
        cascadeType(a->kids[i]);
    }
}

AST *newBinop(const char *kind, AST *l, AST *r) {
    return newAST(kind, 2, l, r);
}

AST *newUnop(const char *kind, AST *l) {
    return newAST(kind, 1, l);
}

void freeAST(AST *a) {
    if (!a) return;
    for (size_t i=0; i<a->nKids; i++) {
        freeAST(a->kids[i]);
    }
    free(a);
}

void printAST(const AST *a, const int d) {
    if (!a) return;
    char *typestr; {
        size_t n;
        Type t = a->type;
        fmtType(&t, &typestr, &n);
    }

    printf("%*s%s of type %s at line %d: ", d, "", a->kind, typestr, a->line);
    free(typestr);
    if (0==strcmp(a->kind, "intlit")) {
        printf("%ld\n", a->ival);
        return;
    }
    if (0==strcmp(a->kind, "reallit")) {
        printf("%lf\n", a->dval);
        return;
    }
    if (0==strcmp(a->kind, "ident")) {
        printf("%s\n", getString(a->str));
        return;
    }
    putchar('\n');
    for (size_t i=0; i<a->nKids; i++) {
        printAST(a->kids[i], d+4);
    }
}

void printToken(const int token, FILE *f) {
    /* single character tokens */
    if (token < 256) {
        if (token < 33) {
            /* non-printable character */
            fprintf(f, "chr(%d)", token);
        } else {
            fprintf(f, "'%c'", token);
        }
        return;
    }
    /* standard tokens (>255) */
    switch (token) {
        case PROGRAM   : fprintf(f, "PROGRAM"); break;
        case CONST     : fprintf(f, "CONST"); break;
        case IDENTIFIER: fprintf(f, "identifier<%s>", getString(yylval.string)); break;
        case VAR       : fprintf(f, "VAR"); break;
        case ARRAY     : fprintf(f, "ARRAY"); break;
        case RANGE     : fprintf(f, ".."); break;
        case INTNUMBER : fprintf(f, "Integer<%ld>", yylval.ival); break;
        case REALNUMBER: fprintf(f, "Real<%lf>", yylval.dval); break;
        case OF        : fprintf(f, "OF"); break;
        case INTEGER   : fprintf(f, "INTEGER"); break;
        case REAL      : fprintf(f, "REAL"); break;
        case FUNCTION  : fprintf(f, "FUNCTION"); break;
        case PROCEDURE : fprintf(f, "PROCEDURE"); break;
        case BEGINTOK  : fprintf(f, "BEGIN"); break;
        case ENDTOK    : fprintf(f, "END"); break;
        case ASSIGN    : fprintf(f, ":="); break;
        case IF        : fprintf(f, "IF"); break;
        case THEN      : fprintf(f, "THEN"); break;
        case ELSE      : fprintf(f, "ELSE"); break;
        case WHILE     : fprintf(f, "WHILE"); break;
        case DO        : fprintf(f, "DO"); break;
        case SKIP      : fprintf(f, "SKIP"); break;
        case READLN    : fprintf(f, "READLN"); break;
        case WRITELN   : fprintf(f, "WRITELN"); break;
        default: fprintf(f, "'UNKNOWN TOKEN'"); break;
    }
}
