%{
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <math.h>

	extern int yylex (void);  /* scanner produced by flex */
	int yyerror(char *s);     /* forward declaration */

	#define BOX(x) (box(sizeof(x),&(x)))

	typedef unsigned char u8;

	void *box(size_t n, void *data){
		u8 *r = calloc(1, n);
		memcpy(r, (u8*)data, n);
		return r;
	}

	typedef struct Synt Synt;
	Synt *parsed = NULL;
	Synt *leaf(double v);
	Synt *binop(int op, Synt *l, Synt *r);
%}

%union {
   double number;
   int    token;
   Synt  *tree;
}

%token <number>PUSH
%token <token> ADD SUB MUL DIV EXP LEFTPAR RIGHTPAR
%type  <tree>  Expr

%left ADD SUB
%left MUL DIV
%right EXP

%start Program

%%

Program: Expr {parsed = $1;}
       ;

Expr: PUSH           {$$ = leaf($1);}
    | Expr ADD Expr  {$$ = binop(ADD, $1, $3);}
    | Expr SUB Expr  {$$ = binop(SUB, $1, $3);}
    | Expr MUL Expr  {$$ = binop(MUL, $1, $3);}
    | Expr DIV Expr  {$$ = binop(DIV, $1, $3);}
    | Expr EXP Expr  {$$ = binop(EXP, $1, $3);}
    | SUB Expr       {$$ = binop(MUL, $2, leaf(-1));}
    | LEFTPAR Expr RIGHTPAR {$$ = $2;}
    ;

%%

int yyerror(char *s) {
  printf("%s\n", s);
  exit(EXIT_SUCCESS);
}

struct Synt {
	Synt *left, *right;
	int tok;
	double val;
};

Synt *leaf(double v){
	return BOX(((Synt){NULL,NULL,PUSH,v}));
}

Synt *binop(int op, Synt *l, Synt *r){
	return BOX(((Synt){l,r,op,0}));
}

static const char *FUCKED = "PARSE TREE ERROR";
const char *tok2str(int tok){
	#define c(x) case x: return #x;
	switch(tok){
		c(PUSH)
		c(ADD)
		c(SUB)
		c(MUL)
		c(DIV)
		c(EXP)
		c(LEFTPAR)
		c(RIGHTPAR)
		default: return FUCKED;
	}
	#undef c
}

typedef void Visitor(Synt *, const char *, int);
typedef enum {postOrder, preOrder} Order;

static void traverseNoop(Synt *t, const char *s, int d){}
static void traverseHelper(Synt *t, Visitor pre, Visitor post, int d){
	if (t==NULL) return;
	const char *tokstr = tok2str(t->tok);
	pre(t,tokstr,d);	
	traverseHelper(t->left,  pre,post,d+1);
	traverseHelper(t->right, pre,post,d+1);
	post(t,tokstr,d);
}

void traverse(Synt *t, Visitor v, Order order){
	if (order == preOrder){
		traverseHelper(t, v, traverseNoop, 0);
	} else {
		traverseHelper(t, traverseNoop, v, 0);
	}
}

void emit(Synt *t, const char *s, int _){
	printf("%s", s);
	if (t->tok == PUSH){
		if (-1 == t->val){
			printf(" -1"); // hrmpf
		} else {
			printf(" %f", t->val);
		}
	}
	puts("");
}

void diag(Synt *t, const char *s, int d){
	printf("%*s%s", d*4,"", s);
	if (t->tok == PUSH){
		printf(" %f", t->val);
	} else if (s == FUCKED){
		printf("(%d)", t->tok);
	}
	puts("");
}

static double stack[9999];
static size_t top = 0;
static void   push(double x){        stack[top++] = x; }
static double pop (void    ){ return stack[--top]    ; }
static void   stackdbg(void){ for(int i=0; i<top; i++){printf("%f%s", stack[i], i+1==top?"\n":", ");} }

void stackExec(Synt *t, const char *s, int d){
	#define b(tok, op) case tok: {double tmp=pop(); push(pop() op tmp); break;}
	switch (t->tok){
		case PUSH: push(t->val); break;
		case EXP: push(pow(pop(),pop())); break;
		b(MUL, *)
		b(DIV, /)
		b(ADD, +)
		b(SUB, -)
	}
	#undef b
}

void freeSynt(Synt *t, const char *_0, int _1){
	free(t);
}

int main() {
	yyparse();
	//traverse(parsed, diag, preOrder);
	traverse(parsed, stackExec, postOrder);
	printf("%f\n", pop());
	traverse(parsed, freeSynt, postOrder);
	return 0;
}