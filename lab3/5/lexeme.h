#ifndef LEXEME_H
#define LEXEME_H

typedef enum {
   tprog,tconst,tvar,tarray,tof,tint,treal,
   tfunc,tproc, treadln,twriteln,
   tbeg,tend,tskip,tif,tthen,telse,twhile,tdo,
   tnot,tor,tand,
   tcolon,tsemi,tcomma,tddot,
   tlp,trp,tlc,trc,tls,trs,
   teq,tgt,tlt,tlg,tge,tle,
   tis,
   tplus,tmin,tstar,tslash,tdiv,tmod,
   tint_lit,treal_lit,tid
} Token;

typedef struct {
   Token token;
   int   line;
   union {
      char  *id_val;
      long   int_val;
      double real_val;
   };
} Lexeme;

#endif
