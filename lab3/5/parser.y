%{
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
//#define YYDEBUG 1
//yydebug = 1;
	extern int int_val;
	extern int yylineno;
	extern double real_val;
	extern char *id_val;
	extern int yylex ();  /* scanner produced by flex */
	int yyerror(const char *s);     /* forward declaration */
%}
%debug

//%define api.pure full
//%define parse.error verbose
//%define parse.lac full

%token PROGRAM CONST VAR ARRAY OF INT REAL
%token FUNCTION PROCEDURE READLN WRITELN
%token SEMICOLON tis tlp trp tdot tddot tcomma tcolon tls trs
%token BISONSUCKSBEGIN END SKIP IF THEN ELSE WHILE DO
%token NOT OR AND
%token teq tgt tlt tlg tge tle
%token tplus tmin tstar tslash tdiv tmod

%token <id_val> ID
%token <int_val> INT_LIT
%token <real_val> REAL_LIT

%union {
   char* id_val;
   long int_val;
   double real_val;
}

%left SEMICOLON
%left tplus tmin
%left tstar tslash tdiv tmod
%left teq tgt tlt tlg tge tle
%left NOT
%left OR AND

%start Program

%%

Num: INT_LIT | REAL_LIT ;
Relop: teq | tgt |tlt | tlg | tge | tle ;

Program:
PROGRAM ID SEMICOLON
ConstDeclRep
VarDeclRep
FuncProcDeclRep
CompoundStatement tdot
;

ConstDeclRep: | ConstDeclRep ConstDecl ;
VarDeclRep: | VarDeclRep VarDecl ;
FuncProcDeclRep: | FuncProcDeclRep FuncProcDecl ;

ConstDecl: CONST ID teq Num SEMICOLON ;
VarDecl: VAR IdentifierList tcolon TypeSpec SEMICOLON ;
IdentifierList: ID | ID tcomma IdentifierList ;
TypeSpec: BasicType | ARRAY tls Num tddot Num trs OF BasicType ;
BasicType: INT | REAL ;
FuncProcDecl
: FUNCTION ID Parameters tcolon BasicType SEMICOLON VarDeclRep CompoundStatement SEMICOLON
| PROCEDURE ID SEMICOLON VarDeclRep CompoundStatement SEMICOLON
| PROCEDURE ID Parameters SEMICOLON VarDeclRep CompoundStatement SEMICOLON;
Parameters: tlp ParameterList trp ;
ParameterList
: VAR IdentifierList tcolon TypeSpec
| IdentifierList tcolon TypeSpec
| ParameterList SEMICOLON ParameterList;
CompoundStatement: BISONSUCKSBEGIN StatementList END | BISONSUCKSBEGIN END ;
StatementList: Statement | StatementList SEMICOLON Statement ;
Statement: Lhs tis ArithExpr
| ProcedureCall
| CompoundStatement
| SKIP
| IF Guard THEN Statement ELSE Statement
| WHILE Guard DO Statement;
Guard: BoolAtom
| NOT Guard
| Guard OR Guard
| Guard AND Guard
| tlp Guard trp;
BoolAtom: ArithExpr Relop ArithExpr ;
Lhs: ID tls ArithExpr trs | ID ;
ProcedureCall: ID | ID tlp ArithExprList trp | READLN tlp ArithExprList trp | WRITELN tlp ArithExprList trp ;
ArithExprList: ArithExpr | ArithExprList tcomma ArithExpr ;
ArithExpr
: ID
| ID ArrayThing
| Num
| ID tlp ArithExprList trp
| ArithExpr tplus ArithExpr
| ArithExpr tmin ArithExpr
| ArithExpr tstar ArithExpr
| ArithExpr tslash ArithExpr
| ArithExpr tdiv ArithExpr
| ArithExpr tmod ArithExpr
| tmin ArithExpr
| tlp ArithExpr trp
;

ArrayThing: tls ArithExpr trs | tls ArithExpr tddot ArithExpr trs;

%%

int yyerror(const char *s) {
  printf("PARSE ERROR (%d)\n", yylineno);
  exit(EXIT_SUCCESS);
}

int main() {
	yyparse();
	puts("ACCEPTED");
	return 0;
}